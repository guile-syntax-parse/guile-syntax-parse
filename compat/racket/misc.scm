;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (compat racket misc)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (system syntax)
  #:use-module (system vm program)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex )
  #:use-module (ice-9 session)
  #:use-module (ice-9 pretty-print)
  #:export (syntax-local-value
            stx->list 
            syntax->pair
            syntax->list
            stx-list?
            stx-car 
            stx-cdr 
            stx-pair?
            stx-null?
            ver
            syntax?
            syntax-e
            syntax-module

            s-sharp 

            dict-map
            dict-for-each
	    
	    rdatum->syntax

            pp
            pkk
            pku

            add1 
           
            ar
            make-syntax-data
            syntax-data-apply
            
            with-pattern-variables
            make-syntax-mapping
            syntax-mapping-ref
            syntax-mapping-set!
            syntax-mapping-valvar
            syntax-pattern-variable?

            syntax-property

            with-disappeared-uses

            aif 
            
            andmap
            ormap
            null

            box
            box?
            unbox
            set-box!

            exact-positive-integer?
            exact-nonnegative-integer?

            struct->list

            make-keyword-procedure
            procedure-arity-includes?

            keyword-apply
            
            remove-duplicates
            
            make-rename-transformer
            rename-transformer?

            record-disappeared-uses
            
            make

            current-syntax-context
            
            rerror
            wrong-syntax
            raise-syntax-error
            raise-mismatch-error
            raise-type-error

            format-id 
            format-symbol

            define-struct
            define-struct-struct
            remake-struct

            list-out
            struct-out
            re-struct-out
            re-export-all
            all-defined-out

            first
            second
            third
            fourth

            void
                        
            u-string-append

            %expression

            identifier-binding

            generate-temporary
            generate-n-temporaries

            check-duplicate-identifier

            keyword<?
            
            fix-mark
            
            syntax-local-phase-level

            class-in-lambda
            is-class?

            conv-in-lambda
            is-syntax-conv?

            litset-in-lambda
            is-syntax-litset?            
            
            let/ec

            syntax-transforming?
            
            syntax-parameter-value
            syntax-local-infer-name

            call-with-immediate-continuation-mark
            with-continuation-mark 
            
            procedure-keywords

            build-source-location
            
            regexp-match
            regexp-replace
            procedure-closure-contents-eq?

            syntax-local-lift-expression
            
            build-list
            
            begin0           

            immutable?
            void?

            partition
            flatten

            memf
            vector-immutable
            box-immutable
            last
            sub1

            quote-module-path
            %%app

            make-arity-at-least
            arity-at-least
            arity-at-least?
            arity-at-least-value

            syntax/loc
            quasisyntax/loc
            ))

(define-record-type arity-at-least
  (make-arity-at-least value)
  arity-at-least?
  (value  arity-at-least-value))

(define old-assoc assoc)
(define (assoc k x)
  (if x
      (old-assoc k x)
      #f))

(define (regexp-match pat x)
  (cond 
   ((regexp? pat)
    (regexp-exec pat x))
   (else
    (regexp-exec (make-regexp pat) x))))

(define (regexp-replace pat target rep)
  (regexp-substitute/global #f pat target 'pre rep 'post))
                                   

(define-syntax aif
  (syntax-rules ()
    ((aif (it) p x y)
     (let ((it p)) (if it x y)))
    ((aif (it) p x)
     (let ((it p)) (if it x)))))

(cond-expand
  (guile-2.0
   (define (syntax-module x)
     (if (syntax? x)
         (let ((dir (cdr (vector-ref x 3))))
           (resolve-module dir))
         #f)))
  
  (guile-2.2
   (define syntax-module (@ (system syntax internal) syntax-module)))
  
  (guile-3.0
   (define syntax-module (@ (system syntax internal) syntax-module))))

(cond-expand
  (guile-2.0
   (define (syntax? x)
     (and (vector? x) (eq? (vector-ref x 0) 'syntax-object))))
  
  (guile-2.2
   (define (syntax? x)
     (or (and (vector? x)
              (> (vector-length x) 0)
              (eq? 'syntax-object (vector-ref x 0)))              
         ((@ (system syntax internal) syntax?) x))))
  
  (guile-3.0
   (define (syntax? x)
     (or (and (vector? x)
              (> (vector-length x) 0)
              (eq? 'syntax-object (vector-ref x 0)))              
         ((@ (system syntax internal) syntax?) x)))))

(cond-expand
  (guile-2.0
   (define syntax-expression (lambda (x) (vector-ref x 1))))
  
  (guile-2.2
   (define (syntax-expression x)
     (if (and (vector? x)
              (> (vector-length x) 0)
              (eq? 'syntax-object (vector-ref x 0)))
         (vector-ref x 1)
         ((@ (system syntax internal) syntax-expression) x))))
  
  (guile-3.0
   (define (syntax-expression x)
     (if (and (vector? x)
              (> (vector-length x) 0)
              (eq? 'syntax-object (vector-ref x 0)))
         (vector-ref x 1)
         ((@ (system syntax internal) syntax-expression) x)))))

(define (syntax-e x)
  (if (syntax? x)
      (syntax-expression x)
      x))

(define (keyword<? x y)
  (pkk 'keyword<?)
  (if (and (keyword? x) (keyword? y))
      (let ((sx (symbol->string (keyword->symbol x)))
            (sy (symbol->string (keyword->symbol y))))
        (string<? sx sy))))

(define* (generate-temporary #:optional [x #'temp])
  (pkk 'generate-temporary)
  (let ((r (syntax->datum x)))
    (if (not (symbol? r)) 
        (set! r 'temp))
    (if (not (syntax? x))
        (set! x #'temp))
    (datum->syntax x (gensym (symbol->string r)))))
  

(define *rename-macro* (make-weak-key-hash-table))

(define (make-rename-transformer y)
  (let ((f (lambda (x)
             (syntax-case x ()
               ((_ . l)  #`(#,y . l))
               (_        y)))))
    
    (hash-set! *rename-macro* f #t)
    f))

(define (rename-transformer? f)
  (and (procedure? f) (hash-ref *rename-macro* f)))

(define-record-type -box
  (box x)
  box?
  (x unbox set-box!))

(define (stx->list x)
  (syntax-case x ()
    ((x . l) (cons #'x (stx->list #'l)))
    (()     '())))

(define (syntax->pair x)
  (syntax-case x ()
    ((x . l) (cons #'x #'l))
    (x #'x)))

(define syntax->list stx->list)
(define (stx-list? x)
  (syntax-case x ()
    ((x . l)
     (stx-list? #'l))
    (() #t)
    (_  #f)))

(define (stx-car x)
  (syntax-case x ()
    ((x . l) #'x)))

(define (stx-cdr x)
  (syntax-case x ()
    ((x . l) #'l)))

(define (stx-pair? x)
  (syntax-case x ()
    [(x . l) #t]
    [_       #f]))

(define (stx-null? x)
  (syntax-case x ()
    [() #t]
    [_  #f]))

(define-syntax-rule (add1 x) (+ x 1))

(define-syntax with-disappeared-uses (syntax-rules () [(_ x) x]))


(define (f x)
  (let-values (((m x) (syntax-local-binding x)))
    (cons m x)))

(define* (syntax-local-value x #:optional [failure #f] [lookup #f])
  (let ((lp (if lookup
                lookup
                f)))
    (let loop ((r (lp x)))
      (if (and (pair? r) (eq? (car r) 'macro))
          (let ((r (cdr r)))
            (if (rename-transformer? r)
                (loop (lp (r #t)))
                r))
          (if failure (failure) #f)))))

  

(define *syntax-map* (make-weak-key-hash-table))

(define (syntax-data-apply f x . l)
  (apply f (hashq-ref *syntax-map* x) l))

(define (make-syntax-data self f)
  (let ((res (lambda (stx)
               (f self stx))))
    (hashq-set! *syntax-map* res self)
    res))

(define-record-type syntax-mapping
  (-make-syntax-mapping ref set!)
  syntax-mapping?
  (ref  syntax-mapping-ref)
  (set! syntax-mapping-set!))

(define (make-syntax-mapping a b)
  (make-syntax-data
   (-make-syntax-mapping a b)
   (lambda (self stx)
     (if (identifier? stx)
         (error
          "pattern variable cannot be used outside of a template"
          )
         (error
          "pattern variable cannot be used outside of a template")))))

          
(define (syntax-mapping-valvar x)
  (syntax-data-apply 
   syntax-mapping-ref
   x))

(define-syntax with-pattern-variable
  (lambda (x)
    (syntax-case x ()
      [(_ f v code)
       #'(let ((sym (gensym "x")))
           (set-symbol-property! sym 'a v)
           (with-syntax ((s (datum->syntax #'f sym)))
             #`(let-syntax ((f (make-syntax-mapping 
                                (symbol-property 's 'a) 
                                #f)))
                 #,code)))])))

(define-syntax with-pattern-variables
  (syntax-rules ()
    ((_ ((f v) . l) code)
     (with-pattern-variable 
      f v 
      (with-pattern-variables l code)))
    ((_ () code) 
     code)))
                            

         
  
(define (syntax-pattern-variable? x)
  (syntax-data-apply
   syntax-mapping?
   x))

(define *syntax-property* (make-weak-key-hash-table))


(define (syntax-property stx k . v)
  (if (null? v)
      (let ((r (assq k (hashq-ref *syntax-property* stx '()))))
        (if r (cdr r) r))
      (let ((r (hashq-ref *syntax-property* stx '())))
        (let ((a   (cons (cons k (car v)) r)))
          (hashq-set! *syntax-property* stx a)
          stx))))

      
(define andmap 
  (case-lambda 
    ((pred l)
     (let loop ((r #t) (l l))
       (if (pair? l)
           (aif (it) (pred (car l))
                (loop it (cdr l))
                #f)
           r)))

    ((pred l1 l2)
     (let loop ((r #t) (l1 l1) (l2 l2))
       (if (and (pair? l1) (pair? l2))
           (aif (it) (pred (car l1) (car l2))
                (loop it (cdr l1) (cdr l2))
                #f)
           r)))))


(define ormap 
  (case-lambda
    ((pred l)
     (if (pair? l)
         (aif (t) (pred (car l))
              t
              (ormap pred (cdr l)))
         #f))
    ((pred l1 l2)
     (if (and (pair? l1) (pair? l2))
         (aif (t) (pred (car l1) (car l2))
              t
              (ormap pred (cdr l1) (cdr l2)))
         #f))))

(define null '())

(cond-expand 
  (guile-2.2)
  (guile-3.0)
  (guile-2
   (define (exact-integer? x)
     (and (number? x)
          (exact? x)
          (integer? x)))
   (export exact-integer?)))

(define (exact-positive-integer? x)  
  (and (number? x)
       (exact? x)
       (integer? x)
       (positive? x)))

(define (exact-nonnegative-integer? x)
  (and (number? x)
       (exact? x)
       (integer? x)
       (not (negative? x))))

(define (struct->list x)
  (let loop ((n 0) (l '()))
    (catch #t
      (lambda () (loop (+ n 1) (cons (struct-ref x n) l)))
      (lambda x  (reverse l)))))

(define* (make-keyword-procedure f #:optional (fplain #f))
  (lambda x
    (let loop ((x x) (kws '()) (args '()))
      (match x
        [((? keyword? k) v . l)
         (loop l (cons (cons k v) kws) args)]
        
        [(x . l)
         (loop l kws (cons x args))]

        [()
         (if (null? kws)
             (if fplain
                 (apply fplain    (reverse args))
                 (apply f '() '() (reverse args)))
             (let* ((kws (sort kws (lambda (a b) (keyword<? (car a) (car b)))))
                    (k   (map car kws))
                    (v   (map cdr kws)))
               (apply f k v (reverse args))))]))))
        

(define (keyword-apply f kws kval . l)
  (apply f (let loop ((kws kws) (kval kval) (r '()))
             (if (pair? kws)
                 (loop (cdr kws)
                       (cdr kval)
                       (cons (car kws) (cons (car kval) r)))
                 (append l r)))))


(define* (remove-duplicates x #:optional (same? equal?))
  (let loop ((x x) (out '()))
    (if (pair? x)
        (let loop2 ((y (cdr x)))
          (if (pair? y)
              (if (same? (car x) (car y))
                  (loop (cdr x) out)
                  (loop2 (cdr y)))
              (loop (cdr x) (cons (car x) out))))
        (reverse out))))


(cond-expand
  (guile-2.2)
  (guile-3.0)
  (guile-2
   (define-syntax define-values
     (lambda (stx)
       (syntax-case stx ()
         [(_ (v ...) code ...)
          (with-syntax ((skunk   (datum->syntax stx (gensym "a")))
                        ((a ...) (generate-temporaries #'(v ...))))
            #'(begin
		(define v #f)
		...
		(define skunk (let-values (((a ...) (begin code ...)))
				(set! v a)
				...
				#f))))])))
  (export define-values)))
                           

(define (record-disappeared-uses x) #t)
           
(define-syntax make
  (lambda (stx)
    (syntax-case stx ()
      [(make f l ...)
       (with-syntax ((cstr (datum->syntax #'f
                                          (string->symbol
                                           (u-string-append
                                            "make-"
                                            (symbol->string
                                             (syntax->datum #'f)))))))
         #'(cstr l ...))])))


(define current-syntax-context (make-fluid))
(fluid-set! current-syntax-context #f)

(define (wrong-syntax stx l . u)
  (error (pkk (string-append
          (format #f "~a, wrong-syntax in syntax-parse ~%" (loc-message stx))
          (apply format #f l u)
          (format #f "~%in syntax~%~a" (syntax->datum stx))))))

(define (clean x)
  (if (syntax? x)
      (clean (syntax-e x))
      (if (pair? x)
          (cons (clean (car x)) (clean (cdr x)))
          x)))

(define (loc-message stx)
  (let ((r (syntax-source stx)))
    (if r
        (let* ((line (assoc 'line r))
               (line (if line (cdr line) "-"))
               (col  (assoc 'column r))
               (col  (if col  (cdr col)  "-"))
               (fn   (assoc 'filename r))
               (fn   (if fn 
                         (if (cdr fn)
                             (car (reverse (string-split (cdr fn) #\/)))
                             "-")
                         "-")))
          (format #f "(~a:~a in ~a)" line col fn))
        "")))

(define* (raise-syntax-error name message 
                             #:optional expr sub-expr extra-sources)
  (let ((a (if name     
               (format #f "(~a)" name) 
               ""))
        (b (if message  
               (if (equal? message "#f") " " message)
               ""))
        (c (if expr     
               (format #f "in expression, ~a" (clean expr))
               ""))
        (d (if sub-expr 
               (format #f "subexpression, ~a" (clean sub-expr)) 
               "")))
        
    (error (format #f "~a, ~a syntax error (syntax parse), ~a~%   ~a~%   ~a~%"
                   (loc-message expr) a b c d))))

(define* (raise-mismatch-error name message 
                               #:optional expr sub-expr extra-sources)
  (let ((a (if name     
               (format #f "(~a)" name) 
               ""))
        (b (if message  
               (if (equal? message "#f") " " message)
               ""))
        (c (if expr     
               (format #f "in expression, ~a" (clean expr))
               ""))
        (d (if sub-expr 
               (format #f "subexpression, ~a" (clean sub-expr)) 
               "")))
    (error (format #f "~a, ~a mismatch error (syntax parse), ~a~%   ~a~%   ~a~%"
                   (loc-message expr) a b c d))))

(define* (raise-type-error name message 
                           #:optional expr sub-expr extra-sources)
  (let ((a (if name     
               (format #f "(~a)" name) 
               ""))
        (b (if message  
               (if (equal? message "#f") " " message)
               ""))
        (c (if expr     
               (format #f "in expression, ~a" (clean expr))
               ""))
        (d (if sub-expr 
               (format #f "subexpression, ~a" (clean sub-expr)) 
               "")))
    (error (format #f "~a, ~a type error (syntax parse), ~a~%   ~a~%   ~a~%"
                   (loc-message expr) a b c d))))

(define (format-id lctx fmt . l)
  (datum->syntax lctx (string->symbol (apply format #f fmt l))))

(define (format-symbol fmt . l)
  (string->symbol
   (apply format #f fmt l)))



(define-syntax define-struct-struct
  (lambda (x)
    (syntax-case x ()
      [(_ name (field ...))
       (let ((nm (syntax->datum #'name)))
         (with-syntax ((q  (format-id #'name "~a-fkns" nm))
                       (mk (format-id #'name "make-~a" nm))
                       (?  (format-id #'name "~a?" nm))
                       ((fn ...)
                        (map (lambda (x)
                               (format-id #'name "~a-~a"
                                          nm (syntax->datum x)))
                             (stx->list #'(field ...))))
                       ((i ...) 
                        (let loop ((n 1) (ns (stx->list #'(field ...))))
                          (if (pair? ns)
                              (cons n (loop (+ n 1) (cdr ns)))
                              '()))))
           
           #'(begin             
               (define q '(q name mk ? fn ...))
               (define-record-type name
                 (mk field ...)
                 ?
                 (field fn) ...))))])))

(define-syntax define-struct
  (lambda (x)
    (syntax-case x ()
      [(_ name (field ...))
       (let ((nm (syntax->datum #'name)))
         (with-syntax ((q  (format-id #'name "~a-fkns" nm))
                       (mk (format-id #'name "make-~a" nm))
                       (?  (format-id #'name "~a?" nm))
                       ((fn ...)
                        (map (lambda (x)
                               (format-id #'name "~a-~a"
                                          nm (syntax->datum x)))
                             (stx->list #'(field ...))))
                       ((n ...) 
                        (let ((m (length (stx->list #'(field ...)))))
                          (let loop ((n 0) (l '()))
                            (if (= n m)
                                (reverse l)
                                (loop (+ n 1) (cons (datum->syntax x (+ n 1))
                                                    l))))))
                       ((i ...) 
                        (let loop ((n 1) (ns (stx->list #'(field ...))))
                          (if (pair? ns)
                              (cons n (loop (+ n 1) (cdr ns)))
                              '()))))
           
           #'(begin             
               (define q '(q name mk ? fn ...))
               (define (name x) (if (syntax? x)
                                    (eq? (syntax->datum x) 'name)
                                    (if (symbol? x)
                                        (eq? x 'name)
                                        #f)))
               (define (mk . l) 
                 (pkk `(make name))
                 (cons #'name l))
               (define (? x) (let ((x (if (syntax? x) (syntax-e x) x)))
                               (and (pair? x) 
                                    (name (car x)))))
               (define (fn x) (and (? x) (list-ref x i)))
               ...)))])))
                 
(define (remake-struct stx s)
  (if (struct? s)
      (let ((args (map (lambda (x) 
                         (if (symbol? x)
                             (datum->syntax stx x)
                             x))
                       (struct->list s)))
            (nm   (struct-vtable-name (struct-vtable s))))
        (with-syntax ((mk      (format-id stx "make-~a" nm))
                      ((a ...) args))
          #'(mk 'a ...)))
      s))

(define-syntax re-export-all
  (syntax-rules ()
    [(_ iface)
     (module-for-each 
      (lambda (name . l)
        (module-re-export! (current-module) (list name)))
      (resolve-interface 'iface))]
    [(_ iface _ li)
     (let ((l 'li))
       (module-for-each 
        (lambda (name . l)        
          (if (not (member name l))
              (module-re-export! (current-module) (list name))))
        (resolve-interface 'iface)))]))

(define (all-defined-out)
  (module-for-each 
   (lambda (name . l)
     (if (not (eq? '%module-public-interface name))
         (module-export! (current-module) (list name))))
   (current-module)))

(define (list-out l) 
  (module-export! (current-module) l))

(define-syntax struct-out 
  (lambda (x)
    (syntax-case x ()
        [(_ nm)
         (with-syntax ((q (format-id #'nm "~a-fkns" (syntax->datum #'nm))))
           #'(module-export! (current-module) q))])))

(define-syntax re-struct-out 
  (lambda (x)
    (syntax-case x ()
      [(_ nm)
       (with-syntax ((id (format-id #'nm "~a-fkns" (syntax->datum #'nm))))
         #'(module-re-export! (current-module) id))])))

(define first  car)
(define second cadr)
(define third  caddr)
(define fourth cadddr)

(define-syntax void
  (syntax-rules ()
    ((_) (if #f #f))))

(define-syntax-rule (%expression x ...) (begin 1 x ...))

(define (check-duplicate-identifier stx)
  (let loop ((ids (stx->list stx)))
    (if (pair? ids)
        (let ((id (car ids)))
          (let loop2 ((iis (cdr ids)))
            (if (pair? iis)
                (let ((id2 (car iis)))
                  (if (bound-identifier=? id id2)
                      id
                      (loop2 (cdr iis))))
                (loop (cdr ids)))))
        #f)))

(define (identifier-binding x . ph)
  (let* ((xx (syntax->datum x)))
    (if (symbol? xx)
        (let ((v (syntax-local-binding x)))
          (if (eq? v 'global)
              (if (and (symbol? xx) (module-defined? (syntax-module x) xx))
                  'global
                  #f)
              (car v)))
        #f)))


(define-syntax-rule (rerror x str a ...)
  (error (format #f "in ~a, ~a"
                 x
                 (format #f str (syntax->datum a) ...))))

(define (generate-n-temporaries n)
  (generate-temporaries
   (let loop ([i 0])
     (if (= i n)
         '()
         (cons
          (string->symbol (format #f "g~sx" i))
          (loop (+ i 1)))))))

(define *pr* #f)
(define (pkk . x) (if *pr* (apply pk x) (car (reverse x))))
(define (pp x . l)
  (if (or *pr* (not (null? l)))
      (begin
        (pretty-print (syntax->datum x))
        x)
      x))

(define (pku x . l)
  (apply pp (let loop ((x x))
              (if (syntax? x)
                  `(syntax ,(loop (syntax-e x)))
                  (if (pair? x)
                      (cons (loop (car x)) (loop (cdr x)))
                      x)))
         l)
  x)
        

(define (dict-map f dict)
  (cond [(null? dict)
         '()]
        [(pair? dict)
         (map (lambda (x) 
                (if (pair? x)
                    (f (caar x) (cdar x))
                    (error "not an assoc")))
              dict)]
        [(hash-table? dict)
         (hash-map->list f dict)]))

(define (dict-for-each f dict)
  (cond [(null? dict)
         '()]
        [(pair? dict)
         (for-each (lambda (x) 
                     (if (pair? x)
                         (f (car x) (cdr x))
                         (error "not an assoc")))
                   dict)]
        [(hash-table? dict) 
         (hash-for-each f dict)]))



(define (u-string-append . l)
  (for-each (lambda (s) 
              (if (not (string? s))
                  (format #t "~a is not a string" s)))
            l)
  (apply string-append l))

   (define (s-sharp x)
     (let ((q (if (syntax? x) (syntax->datum x) x)))
       (if (struct? q)
           (let ((nm (struct-vtable-name (struct-vtable q)))
                 (l  (struct->list q)))
             `(,nm ,@l))
           x)))

(cond-expand
  (guile-2.0
   (define (fix-mark x)
     (define (unshift x)
       (match x
         (('shift . l)
          (unshift l))
         ((a . l)
          (let ((w (unshift l)))
            (if (eq? w l)
                x
                (cons a w))))
         (() '())))

     (match x
       [#(x y ((#f . top) . l) z)
        (vector x y `(,top ,@(unshift l)) z)]
       [#(a b (c . l) m)
        (let ((w (unshift l)))
          (if (eq? w l)
              x
              (vector a b (cons c w) m)))])))
  (guile-2.2
   (define (fix-mark x)
     (define (unshift x)
       (match x
         (('shift . l)
          (unshift l))
         ((a . l)
          (let ((w (unshift l)))
            (if (eq? w l)
                x
                (cons a w))))
         (() '())))
     (let ((y ((@ (system syntax internal) syntax-expression) x))
           (z ((@ (system syntax internal) syntax-wrap      ) x))
           (w ((@ (system syntax internal) syntax-module    ) x)))
           
       (match z
         [((#f . top) . l) 
          ((@ (system syntax internal) make-syntax)
           y `(,top ,@(unshift l)) w)]
         [(c . l)
          (let ((w (unshift l)))
            (if (eq? w l)
                x
                (((@ (system syntax internal) make-syntax)
                  y (cons c w) w))))]))))

  (guile-3.0
   (define (fix-mark x)
     (define (unshift x)
       (match x
         (('shift . l)
          (unshift l))
         ((a . l)
          (let ((w (unshift l)))
            (if (eq? w l)
                x
                (cons a w))))
         (() '())))
     (let ((y ((@ (system syntax internal) syntax-expression) x))
           (z ((@ (system syntax internal) syntax-wrap      ) x))
           (w ((@ (system syntax internal) syntax-module    ) x)))
           
       (match z
         [((#f . top) . l) 
          ((@ (system syntax internal) make-syntax)
           y `(,top ,@(unshift l)) w)]
         [(c . l)
          (let ((w (unshift l)))
            (if (eq? w l)
                x
                (((@ (system syntax internal) make-syntax)
                  y (cons c w) w))))])))))

(define (syntax-local-phase-level) 0)

(define *stx-class-map* (make-weak-key-hash-table))
(define (class-in-lambda x)
  (let ((x x))
    (let ((ret (lambda (y) (raise-syntax-error #f 
                                               "syntax class is not a macro" 
                                               y x))))    
      (hashq-set! *stx-class-map* ret x)
      ret)))

(define (is-class? x)
  (hashq-ref *stx-class-map* x))

(define *stx-conv-map* (make-weak-key-hash-table))
(define (conv-in-lambda x)
  (let ((x x))
    (let ((ret 
           (lambda (y) 
             (raise-syntax-error 
              #f 
              "syntax class convention is not a macro" 
              y x))))    
      (hashq-set! *stx-conv-map* ret x)
      ret)))

(define (is-syntax-conv? x)
  (hashq-ref *stx-conv-map* x))

(define *stx-litset-map* (make-weak-key-hash-table))
(define (litset-in-lambda x)
  (let ((x x))
    (let ((ret (lambda (y) (raise-syntax-error 
                            #f 
                            "syntax class litset is not a macro" 
                            y x))))    
      (hashq-set! *stx-litset-map* ret x)
      ret)))

(define (is-syntax-litset? x)
  (hashq-ref *stx-litset-map* x))

(when (not (defined? 'cons*))
  (define! 'cons* 'list*)
  (export cons*))


(define-syntax let/ec 
  (syntax-rules ()
    ((_ k body ...)
     (call/cc (lambda (k) body ...)))))

(define (syntax-transforming?)
  (catch #t
    (lambda () (syntax-local-binding #'let/ec) #t)
    (lambda x #f)))

(define syntax-parameter-value 
  (case-lambda 
    ((x)
     (let-values (((key val) (syntax-local-binding x)))
       val))
    ((x %f)
     (let-values (((key val) (syntax-local-binding x)))
       (if (eq? val %f) #f val)))))

(define* (procedure-arity-includes? proc n #:optional x)
  (cond ((procedure-property proc 'arglists)
         (ormap
          (lambda (alist)
            (let* ((man   (length (cdr (assoc 'required alist))))
                   (opt   (length (cdr (assoc 'optional alist))))
                   (rst   (cdr (assoc 'rest alist))))
              (and (<= man n)   
                   (or rst (<= n (+ man opt))))))
          (procedure-property proc 'arglists)))

        ((program? proc) 
         (ormap
          (lambda (arity)
            (let* ((alist (program-arguments-alist proc (+ (car arity) 1)))
                   (man   (length (cdr (assoc 'required alist))))
                   (opt   (length (cdr (assoc 'optional alist))))
                   (rst   (cdr (assoc 'rest alist))))
            (and (<= man n)   
                 (or rst (<= n (+ man opt))))))
          (program-arities proc)))
        
        (else
         (let* ((data (procedure-minimum-arity proc))
                (man  (car data))
                (opt  (cadr data))
                (rst  (caddr data)))
           (and (<= man n)   
                (or rst (<= n (+ man opt))))))))

;; guile has no possition tagg on atoms hence this will not yield
;; interesting values.
(define (syntax-local-infer-name stx)
  (datum->syntax stx
     (or (syntax-property stx 'inferred-name)
         ':)))
     

(define (call-with-immediate-continuation-mark key fkn)
  (fkn (fluid-ref key)))

(define-syntax with-continuation-mark 
  (syntax-rules ()
    ((_ key val code ...)
     (with-fluids ((key val)) code ...))))

(define (get-program-arguments f)
  (aif (it) (procedure-property f 'arglists)
       it
       (map (lambda (args)
              (program-arguments-alist f (+ (car args) 1)))
            (program-arities f))))


(define (procedure-keywords f)  
  (if (procedure? f)
      (values 
       '()         
       (sort
        (let loop ((l
                    (map (lambda (al)
                           (map car
                                (cdr (aif (it) (assoc 'keyword al)
                                          it
                                          (list 1)))))
                         (get-program-arguments f))) (r '()))
          (if (pair? l)
              (let ((ll (car l)))
                (let loop2 ((ll ll) (r r))
                  (if (pair? ll)
                      (if (memq (car ll) r)
                          (loop2 (cdr ll) r)
                          (loop2 (cdr ll) (cons (car ll) r)))
                      (loop (cdr l) r))))
              r))
        keyword<?))
      (error "procedure-keyword got non procedure argument")))

(define (build-source-location . l) #f)

(cond-expand
  (guile-3.0
   (define (procedure-closure-contents-eq? x y)
     (if (and (procedure? x) (procedure? y))
         (if (and (program? x) (program? y))
             (and (eq?    (program-code x) (program-code y))
                  (equal? (program-free-variables x)
                          (program-free-variables y)))
             (eq? x y))
         #f)))

  (guile-2.2
   (define (procedure-closure-contents-eq? x y)
     (if (and (procedure? x) (procedure? y))
         (if (and (program? x) (program? y))
             (and (eq?    (program-code x) (program-code y))
                  (equal? (program-free-variables x)
                          (program-free-variables y)))
             (eq? x y))
         #f)))
 
  (guile-2
   (define (procedure-closure-contents-eq? x y)
     (if (and (procedure? x) (procedure? y))
         (if (and (program? x) (program? y))
             (and (eq?    (program-base x) (program-base y))
                  (equal? (program-bindings x) (program-bindings y)))
             (eq? x y))
         #f))))


(define-syntax syntax-local-lift-expression
  (syntax-rules () ((_ x) x)))

(define (build-list n f)
  (let loop ((i 0) (r '()))
    (if (= i n)
        (reverse r)
        (loop (+ i 1) (cons (f i) r)))))

(define-syntax begin0
  (syntax-rules ()
    ((_ e b ...)
     (call-with-values
         (lambda () e)
       (lambda x
         b ...
         (apply values x))))))
  
(define (immutable? x) #f)
(define (void? x) #f)


(define (partition pred lst)
  (let loop ((r '()) (p '()) (lst lst))
    (if (pair? lst)
        (if (pred (car lst))
            (loop (cons (car lst) r) p (cdr lst))
            (loop r (cons (car lst) p) (cdr lst)))
        (values (reverse r) (reverse p)))))
         

(define (flatten x)
  (reverse
   (let loop ((x x) (r '()))
        (if (pair? x)
            (loop (cdr x) (loop (car x) r))
            (if (null? x)
                r
                (cons x r))))))

(define (vector-immutable . l)
  (warn "vector-immutable not supported, results in mutable vector")
  (apply vector l))

(define (box-immutable x)
  (warn "box-immutable not supported, results in mutable vector")
  (box x))

(define (memf proc lst)
  (let loop ((lst lst) (r '()))
    (if (pair? lst)
        (if (proc (car lst))
            (loop (cdr lst) (cons (car lst) r))
            (loop (cdr lst) r))
        (if (null? r)
            #f
            (reverse r)))))

(define (last ls)
  (if (pair? ls)
      (if (pair? (cdr ls))
          (last (cdr ls))
          (car ls))
      ls))

(define (sub1 x) (- x 1))

(define (quote-module-path) (module-name (current-module)))

(define (%%app f . x) (apply f x))
       


(define-syntax syntax/loc
  (lambda (x)
    (syntax-case x ()
      ((_ loc stx)
       (begin
         (set-source-properties!
          (syntax-e #'stx)
          (source-properties (syntax-e #'loc)))
         #'#'stx)))))

(define-syntax quasisyntax/loc
  (lambda (x)
    (syntax-case x ()
      ((_ loc stx)
       (begin
         (set-source-properties!
          (syntax-e #'stx)
          (source-properties (syntax-e #'loc)))
         #'#`stx)))))

(eval-when (compile load eval)
  (define (ver)
    (let ((v (effective-version)))
      (cond
       ((string-match "^2.0" v)
        'v2.0)
       ((string-match "^2.1" v)
        'v2.1)
       ((string-match "^2.2" v)
        'v2.2)
       ((string-match "^2.3" v)
        'v2.3)
       ((string-match "^2.4" v)
        'v2.4)
       ((string-match "^2.5" v)
        'v2.5)
       ((string-match "^2.9" v)
        'v2.9)
       ((string-match "^3.0" v)
        'v3.0)
       (else #f)))))

(cond-expand
 (guile-2.2
  (define-syntax-rule (fluid-let-syntax . l)
    (syntax-parametrize . l))
  (export fluid-let-syntax))

 (guile-3.0
  (define-syntax-rule (fluid-let-syntax . l)
    (syntax-parametrize . l))
  (export fluid-let-syntax))

 (guile-2)

 (else (error "not supported version")))


(define (rdatum->syntax  stx v)
  (catch #t
	 (lambda () 
	   (if (null? v)
	       #'()
	       (datum->syntax stx v)))
	 (lambda x 
	   (error (format #f "(datum->syntax ~a ~a)" 
			  stx v)))))

#;
(define-syntax rdatum->syntax
  (syntax-rules ()
    ((_ stx v)
     (catch #t
	    (lambda () 
	      (datum->syntax stx v))
	    (lambda x 
	      (error (format #f "(datum->syntax ~a ~a = ~a)" 
			     'stx 'v)))))))
