;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (compat racket match lib)
  #:use-module (compat racket match match)
  #:use-module (compat racket misc)
  #:export (== struct*))

  
(define-match-expander ==
  (syntax-rules (==)
    ((== x) (? (lambda (y) (eequal? x y))))
    ((== x comp)
     (? (lambda (y) (comp x y))))))

(define (mk-struct-pred rtd)
  (lambda (rec)
    (and (struct? rec)
	 (eq? (struct-vtable rec) rtd))))

(define  (mk-acc i)
  (lambda (rec)
    (struct-ref rec i)))

(define-match-expander struct*
  (lambda (x)
    (syntax-case x ()
      ((_ sid ((f pat) ...))
       (with-syntax (((acc ...) (map (lambda (x)
				       (datum->syntax
					#'sid
					(string->symbol
					 (format #f "~a-~a"
						 (syntax->datum #'sid)
						 (syntax->datum x)))))
				     (stx->list #'(f ...)))))
	  #'(? (mk-struct-pred sid) (app acc pat) ...))))))

