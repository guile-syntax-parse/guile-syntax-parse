;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (compat racket match ext)
  #:use-module (compat racket match match)
  #:re-export (pipe splice mvb equal sequence-type 
                    define-match-expander-advanced)
  #:export (vec $ <> rxs))

(define (mk-vec-obj x)
  (cons x 0))

(define (vcar x)
  (vector-ref (car x) (cdr x)))
(define (vcdr x)
  (cons (car x) (+ (cdr x) 1)))
(define (vpair? x)
  (< (cdr x) (vector-length (car x))))
(define (vnull? x)
  (= (cdr x) (vector-length (car x))))
(define vequal? equal?)


(define-match-expander vec
  (syntax-rules (vec :)
    ((vec : x ...)
     (vec (list x ...)))
    ((vec x)
     (app mk-vec-obj (sequence-type (vcar vcdr vpair? vnull? vequal?) x)))))

(define-match-expander $
  (lambda (x)
    (syntax-case x ()
      ((_ s p ...)
       (with-syntax (((n ...) (let loop ((i 0) (r '()) (x #'(p ...)))
				(if (pair? x)
				    (loop (+ i 1) (cons i r) (cdr x))
				    (reverse r)))))
	  #'(? (mk-struct-pred s) (app (mk-acc n) pat) ...))))))

(define-match-expander <>
  (syntax-rules ()
    ((_ f val ...)
     (mvb f val ... (splice)))))

(define-match-expander rxs
  (syntax-rules ()
    ((_ s ...)
     (pipe (regexp s) ...))))
