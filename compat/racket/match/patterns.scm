;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (compat racket match patterns)
  #:use-module (compat racket match match)
  #:re-export (pat lvp lvq mid qp literal it match-cl))
