;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (compat racket match match)
  #:use-module (compat racket misc)
  #:use-module ((ice-9 match) #:renamer (symbol-prefix-proc 'ice-9-))
  #:use-module (syntax parse)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-11)
  #:replace (pipe)
  #:export (match match* define-match-expander define-match-expander-advanced
		  match/values match-lambda match-lambda* match-lambda**
		  match-let match-let-values match-let*-values
		  match-letrec 
		  match-define match-define-values
		  
		  mvb equal splice sequence-type
                  eequal?
		  pat lvp lvq mid qp literal it match-cl))

(define (unsplice f)
  (lambda (x n s syms cc)
    (f x n #f syms cc)))

(define (add-splice f)
  (lambda (x n s syms cc)
    (f x n #t syms cc)))

(define (sym-memb x l)
  (let ((x (syntax->datum x)))
    (or-map (lambda (y)
	      (eq? (syntax->datum y) x))
	    l)))

(define (sym-n x)
  (if (pair? x)
      (let loop ((x (car x)) (l (cdr x)))
        (if (pair? l)	
            (let ((ll (car l)))
              (let loop2 ((x x) (r '()))
                (if (pair? x)
                    (if (sym-memb (car x) ll)
                        (loop2 (cdr x) (cons (car x) r))
                        (loop2 (cdr x) r))
                    (loop r (cdr l)))))
            x))
      '()))

(define (sym-u x)
  (if (pair? x)
      (let loop ((x (reverse x)) (r '()))
        (if (pair? x)
            (let ((xx (car x)))
              (let loop2 ((xx xx) (r r))
                (if (pair? xx)
                    (if (sym-memb (car xx) r)
                        (loop2 (cdr xx) r)
                        (loop2 (cdr xx) (cons (car xx) r)))
                    (loop (cdr x) r))))
            r))
      '()))

(define (find-names x xx)
  (define (p x xx)
    (let ((id (syntax->datum x)))
      (let loop ((xx xx))
        (if (pair? xx)
            (if (eq? id (syntax->datum (car xx)))
                (car xx)
                (loop (cdr xx)))
            (error "BUG in or racket match ideom - missing identifier")))))

  (define (pick x xx)
    (let loop ((x x) (r '()))
      (if (pair? x)
          (loop (cdr x) (cons (p (car x) xx) r))
          (reverse r))))

  (let loop ((xx xx) (r '()))
    (if (pair? xx)
        (loop (cdr xx) (cons (pick x (car xx)) r))
        (reverse r))))

(define (sieve-symbols l stx)
  (define (in-stx x stx)
    (syntax-case stx ()
      ((a . b)
       (or (in-stx x #'b) (in-stx x #'a)))
      (a
       (equal? #'a x))))
  
  (syntax-case l ()
    ((x . l) (let ((r (sieve-symbols #'l stx)))
               (if (in-stx #'x stx)
                   (cons #'x r)
                   r)))
    (() '())))

(define-syntax-class literal
  (pattern #t)
  (pattern #f)
  (pattern x:str)
  (pattern x:number)
  (pattern x:character)
  (pattern x:keyword))

(define-syntax-rule (mk-1 f)
  (lambda (x)
    (syntax-case x ()
      ((_ x)   #'(f x))
      ((_ . l) (error (format #f "~a has only one argument" 'f)))
      (_       #'f))))

(define-syntax-rule (mk-2 f)
  (lambda (x)
    (syntax-case x ()
      ((_ x y)   #'(f x y))
      ((_ . l) (error (format #f "~a has only two arguments" 'f)))
      (_       #'f))))
	       
(define-syntax ccar    (mk-1 car))
(define-syntax ccdr    (mk-1 cdr))
(define-syntax eequal? (mk-2 equal?))
(define-syntax nnull?  (mk-1 null?))
(define-syntax ppair?  (mk-1 pair?))

(define-syntax-class mid
  (pattern s:id
	   #:when (let ((e (symbol->string (syntax->datum #'s))))
		    (not (or (equal? e "...")
			     (equal? e "___")
			     (equal? e "_")
			     (equal? e ".._")
			     (string-match "..[0-9]+" e)
			     (string-match "__[0-9]+" e))))))
		    


(define-syntax-class it
  (pattern s:id
	   #:when (let ((e (symbol->string (syntax->datum #'s))))
		    (or (equal? e "...")
			(equal? e "___")))
	   #:attr code
	   (lambda (x p nms it-nms qs qspl s next syms cc)
	     (iterator-work x p nms it-nms qs qspl s #f next syms cc)))

  (pattern s:id
	   #:when (let ((e (symbol->string (syntax->datum #'s))))
		    (or (string-match "..[0-9]+" e)
			(string-match "__[0-9]+" e)))
		    
	   #:attr code
	   (let* ((e (symbol->string (syntax->datum #'s)))
		  (m (string->number
		      (match:substring
		       (or (string-match "__([0-9]+)"e)
			   (string-match "..([0-9]+)"e))
		       1))))		     
	     (lambda (x p nms it-nms qs qspl s next syms cc)
	       (iterator-work x p nms it-nms qs qspl s m next syms cc)))))

(define-syntax-class pat
  #:description "to alter with set! the pat syntax class"
  (pattern _
	   #:when #f
 	   #:with (names    ...) #'()
	   #:attr splicing #f
	   #:attr code     #f))

(define-syntax-class qp
  #:description "to alter with set! the qp syntax class"
  (pattern _
	   #:when #f
 	   #:with (names    ...) #'()
	   #:attr splicing #f
	   #:attr code     #f))

(define *user-matchers*         (make-weak-key-hash-table))
(define *user-matchers-adv-pat* (make-weak-key-hash-table))
(define *user-matchers-adv-qp*  (make-weak-key-hash-table))

(define-syntax-class user
  (pattern x:id 
	   #:when (aif (it) (syntax-local-value #'x)
		       (hashq-ref *user-matchers* it) 
		       #f)))

(define-syntax-class user-adv-pat
  (pattern x:id 
	   #:when (aif (it) (syntax-local-value #'x)
		       (hashq-ref *user-matchers-adv-pat* it) 
		       #f)))

(define-syntax-class user-adv-qp
  (pattern x:id 
	   #:when (aif (it) (syntax-local-value #'x)
		       (hashq-ref *user-matchers-adv-qp* it) 
		       #f)))
	   
(define-syntax define-match-expander
  (lambda (x)
    (syntax-case x ()
      ((_ name lambda)
       #'(define-syntax name
	   (let ((ret lambda))
	     (hashq-set! *user-matchers* ret ret)
	     ret)))

      ((_ name lambda-tr lambda-syntax)     
       #'(define-syntax name
	   (let ((ret lambda-syntax))
	     (hashq-set! *user-matchers* ret lambda-tr)
	     ret))))))

(define-syntax define-match-expander-advanced
  (lambda (x)
    (syntax-case x (pat qp)
      ((_ name pat lambda)
       #'(define-syntax name
	   (let ((ret lambda))
	     (hashq-set! *user-matchers-adv-pat* ret ret)
	     ret)))

      ((_  name pat lambda-tr lambda-syntax)     
       #'(define-syntax name
	   (let ((ret lambda-syntax))
	     (hashq-set! *user-matchers-adv-pat* ret lambda-tr)
	     ret)))

      ((_ name qp lambda)
       #'(define-syntax name
	   (let ((ret lambda))
	     (hashq-set! *user-matchers-adv-qp* ret ret)
	     ret)))

      ((_  name qp lambda-tr lambda-syntax)     
       #'(define-syntax name
	   (let ((ret lambda-syntax))
	     (hashq-set! *user-matchers-adv-qp* ret lambda-tr)
	     ret))))))

(define-syntax-class derived-pat
  (pattern (~and x (n:user . l) 
		 (~parse p:pat (let ((f (syntax-local-value #'n)))
				 ((hashq-ref *user-matchers* f) #'x))))
	   #:with (names    ...) (sieve-symbols #'(p.names    ...) #'x)
	   #:attr splicing p.splicing
	   #:attr code     p.code))

(define-syntax-class derived-pat-adv
  (pattern (~and x (n:user-adv-pat . l)) 
	   #:attr ll (let ((f (syntax-local-value #'n)))
		      ((hashq-ref *user-matchers-adv-pat* f) #'x))
	   #:with (names    ...) (sieve-symbols (car ll) #'x)
	   #:attr splicing       (cadr  ll)	    
	   #:attr code           (caddr ll)))

(define-syntax-class derived-qp
  (pattern (~and x (n:user . l) 
		 (~parse p:qp (let ((f (syntax-local-value #'n)))
				 ((hashq-ref *user-matchers* f) #'x))))
	   #:with (names    ...) (sieve-symbols #'(p.names    ...) #'x)
	   #:attr splicing p.splicing
	   #:attr code     p.code))

(define-syntax-class derived-qp-adv
  (pattern (~and x (n:user-adv-qp . l)) 
	   #:attr ll (let ((f (syntax-local-value #'n)))
		      ((hashq-ref *user-matchers-adv-qp* f) #'x))
	   #:with (names    ...) (sieve-symbols (car ll) #'x)
	   #:attr splicing       (cadr  ll)	    
	   #:attr code           (caddr ll)))

	   
(define-splicing-syntax-class lvp
  (pattern (~seq p:pat ooo:it q:pat ...)
	   #:with (names ...)    
	   (sym-u #'((p.names    ...) (q.names    ...) ...))
	   #:attr splicing #t
	   #:attr code
	   (lambda (x next s syms cc)
	     (ooo.code x p.code 
		       (stx->list #'(p.names ...))
		       (generate-temporaries #'(p.names ...))
		       q.code q.splicing 
		       s next syms cc)))
  (pattern p:pat
	   #:with (names    ...)  #'(p.names ...)
	   #:attr splicing p.splicing
	   #:attr code (unsplice p.code)))



(define-splicing-syntax-class lvq
  (pattern (~seq p:qp ooo:it q:qp ...)
	   #:with (names ...)    
	   (sym-u #'((p.names    ...) (q.names    ...) ...))
 	   #:attr splicing #t
	   #:attr code
	   (lambda (x next s syms cc)
	     (ooo.code x p.code 
		       (stx->list #'(p.names ...))
		       (generate-temporaries #'(p.names ...))
		       q.code q.splicing s #f next syms cc)))


  (pattern p:qp
	   #:with (names    ...)  #'(p.names ...)
	   #:attr splicing p.splicing
	   #:attr code (unsplice p.code)))



(syntax-class-set! qp
  (pattern i:id
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code 
	   (lambda (x next s syms cc)
	     #`(if (eequal? #,x 'i)
		   #,(cc x next s syms)
		   (#,next))))

  (pattern lit:literal
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code 
	   (lambda (x next s syms cc)
	     #`(if (eequal? #,x lit)
		   #,(cc x next s syms)
		   (#,next))))


  (pattern ((~datum unquote) p:pat)
	   #:with (names    ...) #'(p.names    ...)
	   #:attr splicing #f
	   #:attr code p.code)

  (pattern ((~datum unquote-splicing) ((~datum list) p:lvp ...))
	   #:with (names    ...) (sym-u #'((p.names    ...) ...))
	   #:attr splicing #t
	   #:attr code
	   (lambda (x next s syms cc)
	     (match-list x next p.code p.splicing #t #f syms cc)))

  (pattern ((~datum unquote-splicing) ((~datum list-rest) p:lvp ... r:pat))
	   #:with (names    ...) 
	   (sym-u #'((p.names    ...) ... (r.names    ...)))
	   #:attr splicing #t
	   #:attr code
	   (lambda (x next s syms cc)
	     (match-list x next p.code p.splicing #t r.code syms cc)))

  (pattern ((~datum unquote-splicing) ((~datum quote) q:qp))
	   #:with (names    ...) #'(q.names    ...)
	   #:attr splicing #t
	   #:attr code (add-splice q.code))

  (pattern a:derived-qp
           #:with (names    ...)  #'(a.names    ...)
	   #:attr splicing a.splicing
	   #:attr code a.code)
	   
  (pattern a:derived-qp-adv
           #:with (names    ...)  #'(a.names    ...)
	   #:attr splicing a.splicing
	   #:attr code a.code)
	   

  (pattern #(q:lvq ...)
	   #:with (names    ...)  (sym-u #'((q.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
             (with-syntax ((xx (datum->syntax x (gensym "vector"))))
               #`(if (vector? #,x)
                     (let ((xx (vector->list #,x)))
                       #,(match-list #'xx next q.code q.splicing s #f syms cc))
                     (#,next)))))

  (pattern (q:lvq ...)
	   #:with (names    ...)  (sym-u #'((q.names    ...) ...))
	   #:attr splicing #f
	   #:attr code 
	   (lambda (x next s syms cc)
	     (match-list x next q.code q.splicing s #f syms cc))))



(define pipe          #f)
(define equal         #f)
(define sequence-type #f)
(define mvb           #f)
(define splice        #f)
	   
(syntax-class-set! pat
  #:literals (mvb splice equal pipe sequence-type)
  (pattern s:mid
	   #:with (names    ...)  #'(s)
	   #:attr splicing #f
	   #:attr code 
	   (lambda (x next ss syms cc)
             (if (member #'s syms)
                 #`(if (eequal? s #,x)
                       #,(cc #f next ss syms)
                       (#,next))
                 #`(let ((s #,x)) 
                     #,(cc x next #f (cons #'s syms))))))
  
  (pattern ((~datum var) s:id)
	   #:with (names    ...)  #'(s)
	   #:attr splicing #f
	   #:attr code 
	   (lambda (x next ss syms cc)
             #`(let ((s #,x)) 
                 #,(cc x next #f (cons #'s syms)))))

  (pattern (~datum _)
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code
           (lambda (x next s syms cc)
             (cc x next #f syms)))

  (pattern lit:literal
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     #`(if (eequal? #,x lit)
		   #,(cc x next #f syms)
		   (#,next))))

  (pattern ((~datum quote) datum)
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     #`(if (eequal? #,x (quote datum))
		   #,(cc x next #f syms)
		   (#,next))))

  (pattern ((~datum list) xx:lvp ...)
	   #:with (names    ...)  (sym-u #'((xx.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (match-list x next xx.code xx.splicing s #f syms cc)))

  (pattern ((~datum list-rest) xx:lvp ... p:pat)
	   #:with (names    ...)  
	   (sym-u #'((xx.names    ...) ... (p.names    ...)))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (match-list x next xx.code xx.splicing s p.code syms cc)))

  (pattern ((~datum list-no-order) xx:lvp ...)
	   #:with (names    ...)  (sym-u #'((xx.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (if s (error "permutation cannot handle splicing"))
	     (match-permutations x next xx.code xx.splicing #f syms cc)))

  (pattern ((~datum list-no-order) xx:lvp ... . p:pat)
	   #:with (names    ...)  
	   (sym-u #'((xx.names    ...) ... (p.names    ...)))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (if s (error "permutation cannit handle splicing"))
	     (match-permutations x next xx.code xx.splicing p.code syms cc)))

  (pattern ((~datum vector) xx:lvp ...)
	   #:with (names    ...)  (sym-u #'((xx.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (if s (error "vector cannot be spliced"))
             (with-syntax ((xlist (datum->syntax x (gensym "xlist"))))
               #`(if (vector? #,x)
                     (let ((xlist (vector->list #,x)))
                       #,(match-list #'xlist next xx.code xx.splicing 
                                     #f #f syms cc))
                     (#,next)))))

  (pattern ((~datum cons) xx:pat y:pat)
	   #:with (names    ...)  
	   (sym-u #'((xx.names    ...) (y.names    ...)))
	   #:attr splicing y.splicing
	   #:attr code
	   (lambda (x next s syms cc)
	     (match-list x next (list xx.code) (list xx.splicing) 
			 s y.code syms cc)))
  
  (pattern ((~datum and) xx:pat ...)
	   #:with (names    ...)  (sym-u #'((xx.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (and-work x next xx.code s syms cc)))

  (pattern ((~datum or) xx:pat ...)
	   #:with (names    ...)  (sym-n #'((xx.names ...) ...))
	   #:attr splicing #f
	   #:attr code
           (let ((del-names (find-names #'(names ...) #'((xx.names ...) ...))))
             (lambda (x next s syms cc)
               (with-syntax ((cont (datum->syntax x (gensym "cont"))))
                 #`(let ((cont (lambda (names ...)
                                 #,(cc x next s (append #'(names ...) syms)))))
                     #,(or-work x next xx.code del-names s syms #'cont))))))

  (pattern ((~datum not) xx:pat)
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (not-work x next xx.code s syms cc)))

  (pattern ((~datum app) e:expr)
	   #:with (names    ...)  #'()
	   #:attr splicing        #f
	   #:attr code
	   (lambda (x next s syms cc)
             (with-syntax ((xx (datum->syntax x (gensym "xx"))))
               #`(let ((xx (e #,x)))
                   #,(cc #'xx next syms s)))))
  
  (pattern ((~datum app) e:expr p:pat ... q:pat)
	   #:with (names    ...)  
	   (sym-u #'((p.names    ...) ... (q.names    ...)))
	   #:attr splicing q.splicing
	   #:attr code
	   (lambda (x next s syms cc)
	     #`(let ((xx (e #,x)))
		 #,(pipe-work #'xx next (append p.code (list q.code)) 
                              s syms cc))))
    
  (pattern (mvb e:expr p:pat ... q:pat)
	   #:with (names    ...)  
	   (sym-u #'((p.names    ...) ... (q.names    ...)))
	   #:attr splicing q.splicing
	   #:attr code
	   (lambda (x next s syms cc)
	     (with-syntax (((xx ...) (generate-temporaries #'(p ... q))))
	        #`(let-values (((xx ...) (e #,x)))
		    #,(xxand (append p.code (list q.code)) 
                             (stx->list #'(xx ...)) next s syms cc)))))
    	   
  (pattern ((~datum ?) e:expr p:pat ...)
	   #:with (names    ...)  (sym-u #'((p.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     #`(if (e #,x)
		   #,(and-work x next p.code s syms cc)
		   (#,next))))
  
  (pattern ((~datum quasiquote) q:qp)
	   #:with (names    ...)  #'(q.names    ...)
	   #:attr splicing q.splicing
	   #:attr code q.code)

  (pattern (sequence-type
	    (ccar2 ccdr2 ppair2? nnull2? eequal2?) p:pat)
	   #:with (names    ...)  #'(p.names    ...)
	   #:attr splicing p.splicing
	   #:attr code
	   (lambda (x next s syms cc)
	     #`(fluid-let-syntax
		((ccar    (mk-1 ccar2))
		 (ccdr    (mk-1 ccdr2))
		 (ppair?  (mk-1 ppair2?))
		 (null?   (mk-1 nnull2?))
		 (eequal? (mk-2 eequal2?)))
		#,(p.code x next s syms cc))))

  (pattern (equal eequal2? p:pat)
	   #:with (names    ...)  #'(p.names    ...)
	   #:attr splicing p.splicing
	   #:attr code
	   (lambda (x next s syms cc)
	     #`(fluid-let-syntax
		((eequal? (mk-2 eequal2?)))
		#,(p.code x next s syms cc))))

  (pattern ((~datum regexp) rexp:str p:pat ...)
	   #:with (names    ...)  (sym-u #'((p.names    ...) ...))
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
             (with-syntax ((iit (datum->syntax x (gensym "xx"))))
               #`(aif (iit) (string-match rexp #,x)
                      #,(match-work #'iit 1 p.code next syms cc)
                      (#,next)))))

  (pattern (pipe p:pat ...)
	    #:with (names    ...)  (sym-u #'((p.names    ...) ...))
	    #:attr splicing #f
	    #:attr code
	    (lambda (x next s syms cc)
	      (pipe-work x next p.code s syms cc)))

  (pattern (splice)
	   #:with (names    ...)  #'()
	   #:attr splicing #f
	   #:attr code
	   (lambda (x next s syms cc)
	     (cc x next s syms)))

  (pattern a:derived-pat-adv
           #:with (names    ...)  #'(a.names    ...)
	   #:attr splicing a.splicing
	   #:attr code a.code)

  (pattern a:derived-pat
           #:with (names    ...)  #'(a.names    ...)
	   #:attr splicing a.splicing
	   #:attr code a.code
	   ))

(define (pipe-work x next ps s syms cc)
  (if (pair? ps)
      ((car ps) x next #f syms
       (lambda (x next s syms )
	 (pipe-work x next (cdr ps) s syms cc)))
      (cc x next s syms)))

(define (match-work x n ps next syms cc)
  (with-syntax ((xx (datum->syntax x (gensym "xx"))))
    (if (pair? ps)
        #`(let ((xx (match:substring #,x #,n)))
	  #,((car ps) #'xx next #f syms
	     (lambda (xx nn s syms)
	       (match-work x (+ n 1) (cdr ps) next syms cc))))
        #`(let ((xx (match:substring #,x 0)))
            #,(cc #'xx next #f syms)))))


(define (match-list x next pats spl s last syms cc)
  (let loop ((q pats) (spl spl) (x x) (syms syms))
    (if (null? q)
	(if last
	    (last x next s syms cc)
	    (if s
		(cc x next s syms)
		#`(if (nnull? #,x)
		      #,(cc x next #f syms)
		      (#,next))))
	(if (not (car spl))
            (with-syntax ((xcar (datum->syntax x (gensym "xcar")))
                          (xcdr (datum->syntax x (gensym "xcdr"))))
              #`(if (ppair? #,x)
                    (let ((xcar (ccar #,x))
                          (xcdr (ccdr #,x)))
                      #,((car q) #'xcar next #t syms
                         (lambda (xxx next s syms)
                           (loop (cdr q) (cdr spl) #'xcdr syms))))
                    (#,next)))
	    ((car q) x next #f syms    
	     (lambda (x next s syms)
	       (loop (cdr q) (cdr spl) x syms)))))))

  


(define (make-permutations l ret rest fail cc)
  (define (rev l r)
    (ice-9-match l
      ((x . l) (rev l (cons x r)))
      (() r)))

  (ice-9-match l
    ((x . l) (=> next)
     (make-permutations (append (reverse rest) l)
			(cons x ret) '()  next cc))
    ((x . l)
     (make-permutations l ret (cons x rest) fail cc))

    (l (if (null? rest)
	   (cc  (rev (reverse ret) l) fail #f)
	   (fail)))))
						  
	       
(define (match-permutations x next pats spl last syms cc)
  (with-syntax ((cc2  (datum->syntax x (gensym "cc2")))
		(xx   (datum->syntax x (gensym "xx")))
		(fail (datum->syntax x (gensym "fail")))
		(s    (datum->syntax x (gensym "s"))))
    #`(let ((cc2 (lambda (xx fail s)
                   #,(match-list #'xx #'fail pats spl
                                 #f last syms cc))))
        (make-permutations #,x '() '() #,next cc2))))

(define (and-work x next l s syms cc)
  (ice-9-match l
    ((f)
     (f x next s syms cc))

    ((f . l)
     (f x next s syms (lambda (x* n s* syms) 
                        (and-work x next l s syms cc))))
    (() 
     (cc x next s syms))))

(define (or-work x next l nms s syms cont)
  (ice-9-match l
    ((f)
     (with-syntax (((nm ...) (car nms)))
       (f x next s syms (lambda (x n s syms)
                          #`(#,cont nm ...)))))

    ((f . l)
     (with-syntax (((nm ...) (car nms)))
       (f x #`(lambda () 
                #,(or-work x next l nms s syms cont))
          s syms (lambda (x n s syms)
                   #`(#,cont nm ...)))))
    (() 
     #`(#,next))))

(define (not-work x next f s syms cc)
  (f x #`(lambda () #,(cc x next s syms)) s syms (lambda x #`(#,next))))
    
(define (len pair? cdr l m)
  (let loop ((l l) (i 1))
    (if (pair? l)
	(loop (cdr l) (+ i 1))
	(if (<= m i) i #f))))

(define (iterator-work x p nms it-nms qs qspl s n next syms cc)
  (let ((m   (length qs)))
    (with-syntax ((N         (datum->syntax x (gensym "N")))
                  (i         (datum->syntax x (gensym "i")))
                  (xx        (datum->syntax x (gensym "xx")))
                  (loop      (datum->syntax x (gensym "loop")))
                  (xcar      (datum->syntax x (gensym "xcar"))))
      (let* ((ss   '())

	     (code (p #'xcar next #f syms
		      (lambda (x n s syms)
			(set! ss syms))))

	     (nms   (let loop ((nm nms) (r '()))
		      (if (pair? nm)
			  (let loop2 ((s ss))
			    (if (eq? s syms)
				(loop (cdr nm) r)
				(if (equal? (car s) (car nm))
				    (loop (cdr nm) (cons (car nm) r))
				    (loop2 (cdr s)))))
			  (reverse r)))))

	 (with-syntax (((ns  ...) nms)
		       ((ins ...) (generate-temporaries nms)))

	    #`(let ((N (len ppair? ccdr #,x #,m)))
		(if N
		    (let loop ((i 1) (xx #,x) (ins '()) ...)
		      (if (and #,(if n #`(> i #,n) #'#t) (= i (- N #,m)))
			  (let ((ns (reverse ins)) ...)
			    #,(match-list #'xx next qs qspl #t #f 
					  (append #'(ns ...) syms) cc))
			  (if (ppair? xx)
			      (let ((xcar (ccar xx)))
				#,(p #'xcar next #f syms
				     (lambda (x n s syms)
				       #'(loop (+ i 1)
					       (ccdr xx)
					       (cons ns ins) ...))))
			      (#,next))))
		    (#,next))))))))
		

(define-syntax-class clause
  (pattern ((~datum else) codee ...)
           #:attr code (lambda x #'(begin codee ...))
           #:attr name #f)

  (pattern (p:pat codee ...)
	   #:attr code (lambda (x next s)
			 (p.code x next #f '()
				 (lambda q 
				   #'(begin codee ...))))

	   #:attr name (datum->syntax #'p (gensym "next")))	   

  (pattern (p:pat ((~datum =>) id) codee ...)
	   #:attr code (lambda (x next s)
			 (p.code x next s '()
				 (lambda q 
				   #'(begin codee ...))))

	   #:attr name #'id))

(define (xand ps xs next s syms cc)
  (if (= (length ps) (length xs))
      (let loop ((ps ps) (xs xs) (syms syms))
	(if (pair? ps)
	    ((car ps) (car xs) next #f syms 
             (lambda (x next s syms)
               (loop (cdr ps) (cdr xs) syms)))
	    (cc)))
      (error 
       "bug in rackets match, not the same number of patterns as arguments")))

(define (xxand ps xs next s syms cc)
  (pk ps)
  (if (= (length ps) (length xs))
      (let loop ((ps ps) (xs xs) (syms syms))
	(ice-9-match ps
	 ((ps) 
	  (ps (car xs) next s syms cc))
	 ((ps . pss)
	  (ps (car xs) next #f syms 
              (lambda (x next s syms)
                (loop pss (cdr xs) syms))))))	   
      (error 
       "bug in rackets match, not the same number of patterns as arguments")))

(define-syntax-class (clause* nxs)
  (pattern ((~datum else) codee ...)
           #:attr code (lambda x #'(begin codee ...))
           #:attr name #f)

  (pattern ((p:pat ...) codee ...)
	   #:when (= (length (stx->list #'(p ...))) nxs)
	   #:attr code (lambda (x next s)
			 (xand p.code x next #f '()
			       (lambda q 
				 #'(begin codee ...))))

	   #:attr name (datum->syntax (car #'(p ...)) (gensym "next")))	   

  (pattern ((p:pat ...) ((~datum =>) iid:id) codee ...)
	   #:when (= (length (stx->list #'(p ...))) nxs)
	   #:attr code (lambda (x next s)
			 (xand p.code x next s '()
			       (lambda q 
				 #'(begin codee ...))))

	   #:attr name #'iid))



(define (pp x)
  #;(pretty-print (syntax->datum x))
  x)

(define-syntax match
  (lambda (x)
    (syntax-parse x 
      ((_ v:expr cl:clause ...)
       (pp #`(let ((x v))
	       #,(translate #'x cl.code cl.name
			    #'(error "racket match did not match"))))))))

(define (translate x lis nexts error)
  (if (pair? lis)
      (if (car nexts)
          #`(let ((#,(car nexts) 
                   (lambda () #,(translate x (cdr lis) (cdr nexts) error))))
              #,((car lis) x (car nexts) #f))
          ((car lis) x (car nexts) #f))
      error))

(define-syntax match*
  (lambda (x)
    (syntax-parse x
      ((_ (v:expr ...) (~var cl (clause* (length #'(v ...)))) ...)
       (with-syntax (((x ...) (generate-temporaries #'(v ...))))
	 (pp #`(let ((x v) ...)
		 #,(translate #'(x ...) cl.code cl.name
			      #'(error "racket match did not match")))))))))

;; This syntax class can be helpful in user design macros
;;There is a bug in syntax parse and therefore we will need this
(define-syntax-class help
  (pattern ((pp2:pat ...) (~datum cc))
	   #:with ((names ...) ...) #'((pp2.names ...) ...)
	   #:attr code pp2.code))

(define-syntax-class match-cls
  #:literals (match match*)
  ;; -----------------------------------------------------
  (pattern (match e:id (p:pat (~datum cc)))
	   #:with ((names ...) ...) #'((p.names ...))
	   #:attr code
	   (lambda (next cc)
	     (p.code #'e next #f '() (lambda x (cc)))))

  (pattern (match* (ee:id ...) ((pp:pat ...) (~datum cc)))
	   #:with ((names ...) ...)
	   (list (sym-u #'(((pp.names ...) ...))))
	   #:attr code
	   (lambda (cc next)
	     (xand pp.code #'(ee ...) next #f '()
		   (lambda x (cc)))))
;; -----------------------------------------------------
  (pattern (match e:id (p2:pat (~datum cc)) ...)
	   #:with ((names ...) ...) #'((p2.names ...) ...)
	   #:attr code
	   (map (lambda (p)
		  (lambda (next cc)
		    (p #'e next #f '() (lambda x (cc)))))
		p2.code))
  
  (pattern (match* (ee:id ...) pp2:help ...)
	   #:with ((names ...) ...)
	   (map sym-u #'(((pp2.names ...) ...) ...))
	   #:attr code
	   (map (lambda (ps)
		  (lambda (cc next)
		    (xand ps #'(ee ...) next #f '() (lambda x (cc)))))
		pp2.code)))
;; -----------------------------------------------------

(define (ps->sym ps)
  (let ((n (length (stx->list ps))))
    (let loop ((r '()) (i 0))
      (if (= i n)
	  r
	  (loop (cons (datum->syntax ps (gensym "x"))
		      r)
		(+ i 1))))))

(define-syntax match/values
  (lambda (x)
    (syntax-parse x
      ((~and (_ _ . l) (_ e ((p ...) code ...) . body))
       (with-syntax 
	(((n ...) (ps->sym #'(p ...))))
	#'(let-values (((n ...) e))
	    (match* (n ...) . l)))))))

(define-syntax-rule (match-lambda clause ...)
  (lambda (id) (match id clause ...)))

(define-syntax-rule (match-lambda* clause ...)
  (lambda id (match id clause ...)))


(define-syntax match-lambda** 
  (lambda (x)
    (syntax-parse x
      ((~and (_ . l) (_ ((p ...) code ...) . ll))
       (with-syntax 
	(((n ...) (ps->sym #'(p ...))))
	#'(lambda (n ...) (match* (n ...) . l)))))))

(define-syntax-rule (match-let ((p e) ...) body ...)
  (match* (e ...) ((p ...) body ...)))

(define-syntax match-let* 
  (syntax-rules ()
    ((_ ((p e) . l) . body)
     (match e (p (match-let* l . body))))
    ((_ () body ...)
     #'(begin body ...))))


(define-syntax match-let-values 
  (lambda (x)
    (syntax-parse x
      ((_ (((p ...) e) ...) body ...)
       (with-syntax ((((n ...) ...) (map ps->sym 
					 (stx->list #'((p ...) ...)))))
	  #'(let-values (((n ...) e) ...)
	      (match* (n ... ...) ((p ... ...) body ...))))))))


(define-syntax match-let*-values 
  (lambda (x)
    (syntax-parse x
      ((_ (((p ...) e) ...) body ...)
       (with-syntax ((((n ...) ...) (map ps->sym 
					 (stx->list #'((p ...) ...)))))
	  #'(let*-values (((n ...) e) ...)
	      (match* (n ... ...) ((p ... ...) body ...))))))))
  

;TODO this can be done more effectiviely also adding syntax information
;correctly might be needed see racket source code


(define-syntax match-define
  (lambda (x)
    (syntax-parse x 
      ((_ p  e)
       (syntax-parse #'(match e (p cc))
	(m:match-cls
	 #`(define-values (m.names ... ...)
	   (let (e v)
	     #,(m.code #'e (lambda () (error "no match in define-values"))
		       (lambda () #'(values m.names ... ...)))))))))))

(define-syntax match-letrec
  (lambda (x)
    (syntax-parse x 
      ((_ ((p e) ...) body ...)
       #'(let()
	   (match-define p e) ...
	   body ...)))))


(define-syntax match-define-values
  (lambda (x)
    (syntax-parse x 
      ((_ p q ... e)      
       (with-syntax (((ep eq ...) (generate-temporaries #'(p q ...))))
	  (syntax-parse #'(match* (ep eq ...) ((p q ...) cc))
	    (m:match-cls
	     #'(define-values (m.names ... ...)
		 (let ((fail (lambda () 
			       (error "match-define-values does not match"))))
		   (let-values (((ep eq ...) e))
		     #,(m.code #'fail (lambda () 
					#'(values m.names ... ...)))))))))))))

;;TODO improve the interface to this higher level code consider writing code
;;like

  


