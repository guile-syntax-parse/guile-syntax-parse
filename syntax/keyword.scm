;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax keyword)
  #:use-module (compat racket misc)
  #:export (parse-keyword-options
            parse-keyword-options/eol
            options-select
            options-select-row
            options-select-value

            check-expression
            check-identifier
            check-stx-boolean
            check-stx-string
            check-stx-listof))
#|
#lang scheme/base

;; No-contract version...

(require syntax/stx
         scheme/dict)
(provide parse-keyword-options
         parse-keyword-options/eol
         options-select
         options-select-row
         options-select-value

         check-expression
         check-identifier
         check-stx-boolean
         check-stx-string
         check-stx-listof)
|#
;; Parsing keyword arguments

;; KeywordTable = (listof (cons keyword (listof CheckProc)))
;; Options = (listof (list* keyword syntax-keyword (listof any)))

;; CheckProc = syntax syntax -> any
;; The first arg is syntax to check, second arg is context.

;; incompatible-handler : keyword keyword Options syntax syntax -> (values Options syntax)
(define (default-incompatible kw1 kw2 chunks stx ctx)
  (if (eq? kw1 kw2)
      (raise-syntax-error #f "duplicate keyword option" ctx (stx-car stx))
      (raise-syntax-error 
       #f
       (format #f
               "~s option not allowed after ~s option" kw2 kw1)
       ctx (stx-car stx))))

;; too-short-handler : keyword Options syntax syntax -> (values Options syntax)
(define (default-too-short kw chunks stx ctx)
  (raise-syntax-error #f "too few arguments for keyword" ctx (stx-car stx)))

;; not-in-table-handler : keyword syntax syntax -> (values Options syntax)
(define (default-not-in-table kws) 
  (lambda (kw stx ctx)
    (raise-syntax-error #f
                        (format #f "unexpected keyword, expected one of ~s" kws)
                        ctx (stx-car stx))))

;; not-eol-handler : Options syntax syntax -> (values Options syntax)
(define (default-not-eol chunks stx ctx)
  (raise-syntax-error #f
                      "terms left over after keyword options"
                      ctx
                      stx))

(define* (parse-keyword-options/eol stx table
                                   #:key
                                   [context         #f]
                                   [no-duplicates?  #f]
                                   [incompatible    null]
                                   [on-incompatible default-incompatible]
                                   [on-too-short    default-too-short]
                                   [on-not-in-table
                                    (default-not-in-table (map car table))]
                                   [on-not-eol      default-not-eol])
  (define-values (chunks rest)
    (parse-keyword-options stx table
                           #:context         context
                           #:no-duplicates?  no-duplicates?
                           #:incompatible    incompatible
                           #:on-incompatible on-incompatible
                           #:on-too-short    on-too-short
                           #:on-not-in-table on-not-in-table))
  (if (stx-null? rest)
      chunks
      (on-not-eol chunks stx context)))

(define (list-ne-tails lst)
  (if (pair? lst)
      (cons lst (list-ne-tails (cdr lst)))
      null))

;; parse-keyword-options : syntax KeywordTable ... -> (values Options syntax)
;; incompatible-handler is also used for duplicates (same kw arg)
;; incompatible is (listof (list keyword keyword)); reflexive closure taken
(define* (parse-keyword-options stx table
                                #:key
                                [context         #f]
                                [no-duplicates?  #f]
                                [incompatible    null]
                                [on-incompatible default-incompatible]
                                [on-too-short    default-too-short]
                                [on-not-in-table
                                 (default-not-in-table (map car table))])
  (define interfere-table
    (let ([table (make-hash-table)])
      (for-each
       (lambda (entry)
         (for-each (lambda (tail)
                     (for-each 
                      (lambda (next)
                        (hash-set! table 
                                   (list (car tail) next) #t)
                        (hash-set! table 
                                   (list next (car tail)) #t))
                      (cdr tail)))
                   (list-ne-tails entry)))
       incompatible)
      table))

  (define (interferes kw seen)
    (ormap (lambda (seen-kw)
             (and (hash-ref interfere-table (list seen-kw kw) #f)
                  seen-kw))
           (hash-map->list (lambda (k v) k) seen)))

  (define (loop stx rchunks seen)
    (syntax-case stx ()
      [(kw . more)
       (keyword? (syntax-e #'kw))
       (let* ([kw-value (syntax-e #'kw)]
              [entry    (assq kw-value table)])
         (cond [(and no-duplicates?
                     (hash-ref seen kw-value #f))
                (on-incompatible kw-value kw-value 
                                      (reverse rchunks) stx context)]
               [(interferes kw-value seen) =>
                (lambda (seen-kw)
                  (on-incompatible seen-kw 
                                   kw-value (reverse rchunks) stx context))]
               [entry
                (let* ([arity (cdr entry)]
                       [args+rest (stx-split #'more arity)])
                  (if args+rest
                      (let ([args (map (lambda (arg proc)
                                         (proc arg context))
                                       (car args+rest) arity)]
                            [rest (cdr args+rest)])
                        (loop rest
                              (cons (cons* kw-value #'kw args) rchunks)
                              (begin (hash-set! seen kw-value #t) seen)))
                      (on-too-short kw-value (reverse rchunks) stx context)))]
               [else
                (on-not-in-table kw-value stx context)]))]
      [_
       (values (reverse rchunks) stx)]))
  (loop stx null (make-hash-table)))

;; stx-split : stx (listof any) -> (cons (listof stx) stx)
(define (stx-split stx arity)
  (define (loop stx arity acc)
    (cond [(null? arity)
           (cons (reverse acc) stx)]
          [(stx-pair? stx)
           (loop (stx-cdr stx) (cdr arity) (cons (stx-car stx) acc))]
          [else #f]))
  (loop stx arity null))

;; options-select : Options keyword -> (listof (listof any))
(define (options-select chunks kw)
  (let loop ([chunks chunks])
    (if (pair? chunks)
        (let ((chunk (car chunks)))
          (if (eq? kw (car chunk))
              (cons (cddr chunk) (loop (cdr chunks)))
              (loop (cdr chunks))))
        '())))


;; options-select-row : Options keyword -> any
(define* (options-select-row chunks kw #:key (default '()))
  (let ([results (options-select chunks kw)])
    (cond [(null? results)
           default]
          [(null? (cdr results))
           (car results)]
          [else
           (error (format #f 
                          "error in ~a~%multiple occurrences of ~s keyword" 
                          'options-select-row kw))])))

;; options-select-value : Options keyword -> any
(define* (options-select-value chunks kw #:key [default '()])
  (let ([results (options-select chunks kw)])
    (cond [(null? results)
           default]
          [(null? (cdr results))
           (let ([row (car results)])
             (cond [(null? row)
                    (rerror 'options-select-value
                            "keyword ~s has no arguments" kw)]
                   [(null? (cdr row))
                    (car row)]
                   [else
                    (rerror 'options-select-value
                            "keyword ~s has more than one argument" kw)]))]
          [else
           (rerror 'options-select-value
                   "multiple occurrences of ~s keyword" kw)])))

;; Check Procedures

;; check-identifier : stx stx -> identifier
(define (check-identifier stx ctx)
  (unless (identifier? stx)
    (raise-syntax-error #f "expected identifier" ctx stx))
  stx)

;; check-expression : stx stx -> stx
(define (check-expression stx ctx)
  (when (keyword? (syntax-e stx))
    (raise-syntax-error #f "expected expression" ctx stx))
  stx)

;; check-stx-string : stx stx -> stx
(define (check-stx-string stx ctx)
  (unless (string? (syntax-e stx))
    (raise-syntax-error #f "expected string" ctx stx))
  stx)

;; check-stx-boolean : stx stx -> stx
(define (check-stx-boolean stx ctx)
  (unless (boolean? (syntax-e stx))
    (raise-syntax-error #f "expected boolean" ctx stx))
  stx)

#|
;; check-nat/f : stx stx -> stx
(define (check-nat/f stx ctx)
  (let ([d (syntax-e stx)])
    (unless (or (eq? d #f) (exact-nonnegative-integer? d))
      (raise-syntax-error #f "expected exact nonnegative integer or #f" ctx stx))
    stx))
|#

;; check-stx-listof : (stx stx -> A) -> stx stx -> (listof A)
(define (check-stx-listof check) 
  (lambda (stx ctx)
    (unless (stx-list? stx)
      (raise-syntax-error #f "expected list" ctx stx))
    (map (lambda (x)
           (check x ctx))
         (stx->list stx))))
