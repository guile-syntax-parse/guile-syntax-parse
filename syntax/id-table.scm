;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax id-table)
  #:use-module (ice-9 match)
  #:use-module (compat racket misc))

#|
#lang racket/base
(require (for-syntax racket/base
                     unstable/syntax)
         racket/contract/base
         racket/dict)
|#

;; ========

(define (alist-set identifier=? l0 id v)
  ;; To minimize allocation
  ;;   - add new pairs to front
  ;;   - avoid allocation on idempotent sets
  (let* ([not-found? #f]
         [new-l
          (let loop ([l l0])
            (cond [(null? l) (begin (set! not-found? #t) null)]
                  [(identifier=? (caar l) id)
                   (if (eq? v (cdar l)) ;; idempotent; just leave it alone
                       l
                       (cons (cons id v) (cdr l)))]
                  [else
                   (let ([rest* (loop (cdr l))])
                     (if (eq? (cdr l) rest*)
                         l
                         (cons (car l) rest*)))]))])
    (if not-found?
        (cons (cons id v) l0)
        new-l)))

(define (alist-remove identifier=? l0 id)
  ;; To minimize allocation
  ;;   - avoid allocation on idempotent removes
  (let loop ([l l0])
    (cond [(null? l) null]
          [(identifier=? (caar l) id)
           (cdr l)]
          [else
           (let ([rest* (loop (cdr l))])
             (if (eq? (cdr l) rest*)
                 l
                 (cons (car l) rest*)))])))

(define not-given (gensym "not-given"))

;; ========

(define-syntax make-code (lambda (stx)
  (syntax-case stx ()
    [(_ idtbl identifier->symbol identifier=? make-hash-table)
     (with-syntax ([mutable-idtbl
                    (format-id #'idtbl "mutable-~a" (syntax-e #'idtbl))]
                   [make-idtbl
                    (format-id #'idtbl "make-~a" (syntax-e #'idtbl))]
                   [make-mutable-idtbl
                    (format-id #'idtbl "make-mutable-~a" (syntax-e #'idtbl))]
                   [mutable-idtbl?
                    (format-id #'idtbl "mutable-~a?" (syntax-e #'idtbl))])
       (define (s x) (format-id #'idtbl "~a~a" (syntax-e #'idtbl) x))
       (with-syntax ([idtbl?         (s '?)]
                     [idtbl-hash     (s '-hash)]
                     [idtbl-phase    (s '-phase)]
                     [idtbl-ref      (s '-ref)]
                     [idtbl-set!     (s '-set!)]
                     [idtbl-remove!  (s '-remove!)]
                     [idtbl-count    (s '-count)]
                     [idtbl-iterate-first (s '-iterate-first)]
                     [idtbl-iterate-next  (s '-iterate-next)]
                     [idtbl-iterate-key   (s '-iterate-key)]
                     [idtbl-iterate-value (s '-iterate-value)]
                     [idtbl-map      (s '-map)]
                     [idtbl-for-each (s '-for-each)])
         #'(begin

             ;; Struct defs at end, so that dict methods can refer to earlier procs

             (define* (make-idtbl #:optional [init-dict null])
               ;; init-dict is good candidate for object/c like dict/c
               (let ([t (make-hash-table)])
                 (dict-for-each (lambda (k v) 
                   (unless (identifier? k)
                     (raise-type-error 'make-idtbl
                            "dictionary with identifier keys" init-dict))
                   (idtbl-set! t k v))
                                init-dict)
                 t))

             
             (define* (idtbl-ref d id #:optional [default not-given])
               (let ([i (ormap (lambda (i)                                  
                                 (and (identifier=? (car i) id) 
                                      i))
                               (hashq-ref d
                                          (identifier->symbol id)
                                          null))])
                 (if i
                     (cdr i)
                     (cond [(eq? default not-given)
                            (rerror 'idtbl-ref "no mapping for ~a" id)]
                           [(procedure? default) 
                            (default)]
                           [else 
                            default]))))

             (define (idtbl-set! d id v)
               (let* ([sym (identifier->symbol id)]
                      [l   (hashq-ref d sym null)])
                  (hashq-set! d
                              sym
                              (alist-set identifier=? l id v))
                  d))

             (define (idtbl-remove! d id)
               (let* ([sym  (identifier->symbol id)]
                      [l    (hashq-ref d sym null)]
                      [newl (alist-remove identifier=? l id)])
                 (if (pair? newl)
                     (hashq-set! d sym newl)
                     (hashq-remove! d sym))))

             (define (idtbl-count d)
               (apply + (hash-map->list  
                         (lambda (k v) (length v))
                         d)))

             (define (idtbl-for-each d p)
               (define (pp i) (p (car i) (cdr i)))
               (hash-for-each (lambda (k v) (for-each pp v))
                              d))
                              

             (define (idtbl-map f d)
               (define (fp i) (f (car i) (cdr i)))
               (apply append
                      (hash-map->list                        
                       (lambda (k v) (map fp v))
                       d)))


             (list-out
              '(
                make-idtbl
                idtbl?
                idtbl-ref
                idtbl-set!
                idtbl-remove!
                idtbl-count
                idtbl-map
                idtbl-for-each)))))])))

(define (bound-identifier->symbol id) (syntax-e id))

(define (bd x y)             
  (bound-identifier=? (fix-mark x) (fix-mark y)))
       

(make-code bound-id-table
           bound-identifier->symbol
           bd 
	   make-hash-table)

(make-code weak-bound-id-table
           bound-identifier->symbol
           bd 
	   make-weak-key-hash-table)

(define (free-identifier->symbol id) (syntax-e id))

(make-code weak-free-id-table
           free-identifier->symbol
           free-identifier=?
	   make-weak-key-hash-table)

(make-code free-id-table
           free-identifier->symbol
           free-identifier=?
	   make-hash-table)
