;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src with)
  #:use-module (syntax parse src sc)
  #:use-module (compat racket misc)
  #:use-module (syntax parse src litconv)
  #:export (with-syntax-data))


(define-syntax with-syntax-data
  (syntax-rules (class splicing conventions literal-set)
    ((_ () code ...)
     (begin code ...))

    ((_ ((class       u ...) . l) . code)
     (with-syntax-class ((u ...)) 
      (with-syntax-data l . code)))
 
    ((_ ((splicing    u ...) . l) . code)
     (with-syntax-class-splicing ((u ...)) 
       (with-syntax-data l . code)))

    ((_ ((conventions u ...) . l) . code)
     (with-syntax-convention ((u ...)) 
       (with-syntax-data l . code)))

    ((_ ((literal-set u ...) . l) . code)
     (with-syntax-litset ((u ...)) 
       (with-syntax-data l . code)))))
                        
