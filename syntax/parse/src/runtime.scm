;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src runtime)
  #:use-module (compat racket misc)
  #:use-module (syntax parse src runtime-progress)
  #:use-module (syntax parse src runtime-failure)
  #:use-module (syntax parse src rep-data)
  #:use-module (syntax parse src rep-attrs)
  #:use-module ((system syntax) #:select (syntax-local-binding))
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:export (this-syntax
            this-context-syntax
            
            stx-list-take
            stx-list-drop/cx

            let-attributes
            let-attributes*
            let/unpack
            attribute
            attribute-binding
            check-list^depth))

(re-export-all (syntax parse src runtime-progress))
(re-export-all (syntax parse src runtime-failure))

#|
#lang racket/base
(require racket/list
         racket/stxparam
         "runtime-progress.rkt"
         "runtime-failure.rkt"
         (for-syntax racket/base
                     racket/list
                     syntax/kerncase
                     racket/private/sc
                     unstable/syntax
                     "rep-data.rkt"
                     "rep-attrs.rkt"))

(provide (all-from-out "runtime-progress.rkt")
         (all-from-out "runtime-failure.rkt")

         this-syntax
         this-context-syntax

         stx-list-take
         stx-list-drop/cx

         let-attributes
         let-attributes*
         let/unpack
         attribute
         attribute-binding
         check-list^depth)
|#

;; == Syntax Parameters

;; this-syntax
;; Bound to syntax being matched inside of syntax class

(cond-expand
 #;(guile-2
  (define-syntax fail-handler
    (lambda (stx)
      (wrong-syntax stx "internal error: used out of context")))

  (define-syntax cut-prompt
    (lambda (stx)
      (wrong-syntax stx "internal error: used out of context")))

  (define-syntax this-syntax
    (lambda (stx)
      (wrong-syntax stx 
                    "used out of context: not within a syntax class")))

  ;; this-context-syntax
  ;; Bound to (expression that extracts) context syntax 
  ;; (bottom frame in progress)
  (define-syntax this-context-syntax
    (lambda (stx)
      (wrong-syntax stx 
                    "used out of context: not within a syntax class"))))
 ((or guile-2.2 guile-2)
  (define-syntax-parameter fail-handler
    (lambda (stx)
      (wrong-syntax stx "internal error: used out of context")))

  (define-syntax-parameter cut-prompt
    (lambda (stx)
      (wrong-syntax stx "internal error: used out of context")))

  (define-syntax-parameter this-syntax
    (lambda (stx)
      (wrong-syntax stx 
                    "used out of context: not within a syntax class")))

  ;; this-context-syntax
  ;; Bound to (expression that extracts) context syntax 
  ;; (bottom frame in progress)
  (define-syntax-parameter this-context-syntax
    (lambda (stx)
      (wrong-syntax stx 
                    "used out of context: not within a syntax class"))))
 (else
  (error "not suppoerted version of guile")))

;; == with ==

(export with)

(define-syntax renamer
  (lambda (x)
    (syntax-case x ()
      [(_ v) 
       (with-syntax ((*** (datum->syntax x '...)))
         #`(lambda (x)
             (syntax-case x ()
               [(x a ***) #'(v a ***)]
               [_         #'v])))])))

(define-syntax with 
  (lambda (stx)
    (syntax-case stx ()
      [(with ([stxparam expr] ...) . body)
       (with-syntax ([(var ...) (generate-temporaries #'(stxparam ...))])
         (syntax/loc stx
           (let ([var expr] ...)
             (syntax-parameterize ((stxparam (renamer var))
                                ...)
                               . body))))])))

;; == Control information ==

(export fail-handler
         cut-prompt
         wrap-user-code

         fail
         try)

(define-syntax-rule (wrap-user-code e)
  (with ([fail-handler #f]
         [cut-prompt   #t])
        e))

(define-syntax-rule (fail fs)
  (fail-handler fs))


(define-syntax try 
  (lambda (stx)
  (syntax-case stx ()
    [(try e0 e ...)
     (with-syntax ([(re ...) (reverse (stx->list #'(e ...)))])
       (with-syntax ([(fh ...) (generate-temporaries #'(re ...))])
         (with-syntax ([(next-fh ...) (drop-right (stx->list 
                                                   #'(fail-handler fh ...)) 1)]
                       [(last-fh) (take-right (stx->list 
                                               #'(fail-handler fh ...)) 1)])
           #'(let* ([fh (lambda (fs1)
                          (with ([fail-handler
                                  (lambda (fs2)
                                    (next-fh (cons fs1 fs2)))])
                            re))]
                    ...)
               (with ([fail-handler last-fh])
                     e0)))))])))

;; -----


(define (stx-list-take stx n)
  (let loop ([stx stx] [n n])
    (if (zero? n)
        null
        (cons (stx-car stx)
              (loop (stx-cdr stx) (- n 1))))))

;; stx-list-drop/cx : stxish stx nat -> (values stxish stx)
(define (stx-list-drop/cx x cx n)
  (let loop ([x x] [cx cx] [n n])
    (if (zero? n)
        (values x
                (if (syntax? x) x cx))
        (loop (stx-cdr x)
              (if (syntax? x) x cx)
              (- n 1)))))

;; == Attributes

(define-record-type attribute-mapping
  (-make-attribute-mapping var name depth syntax?)
  attribute-mapping?
  (var     attribute-mapping-var)
  (name    attribute-mapping-name)
  (depth   attribute-mapping-depth)
  (syntax? attribute-mapping-syntax?))

#;(define-struct attribute-mapping (var name depth syntax?))

(define (make-attribute-mapping a b c d)
  (make-syntax-data
   (-make-attribute-mapping a b c d)
   (lambda (self stx)
     (if (attribute-mapping-syntax? self)
         (attribute-mapping-var self)
         #`(let ([value #,(attribute-mapping-var self)])
             (if (check-syntax '#,(attribute-mapping-depth self) value)
                 value
                 (error
                  (format #f "attribute is bound to non-syntax value: ~a" 
                          value))))))))
      


;; check-syntax : nat any -> boolean
;; Returns #t if value is a (listof^depth syntax)
(define (check-syntax depth value)
  (if (zero? depth)
      (syntax? value)
      (and (list? value)
           (let loop ((x value))
             (if (pair? x)
                 (and 
                  (check-syntax (- depth 1) (car x))
                  (loop (cdr x)))
                 #t)))))

(define *nonsyntax-attribute* (make-weak-key-hash-table))

(define (nonsyntax-attribute-value name value)
  (let ((ret (lambda (x) 
               (raise-syntax-error 'attribute (format #f "tries to use attribute as a macro for ~a" (syntax->datum name)) x))))
    (hashq-set! *nonsyntax-attribute* ret (list value))
    ret))

(define (attribute? x)
  (hashq-ref *nonsyntax-attribute* x))
    

(define-syntax let-attributes 
  (lambda (stx)
  (define (parse-attr x)
    (syntax-case (s-sharp x) (attr)
      [(attr name depth syntax?) #'(name depth syntax?)]))

  (syntax-case stx ()
    [(let-attributes () . body)
     #'(begin . body)]
    [(let-attributes ([a value] l ...) . body)
     (with-syntax ([(name depth syntax?)
                    (parse-attr #'a)]
                   [ddd (datum->syntax stx '...)]
                   [bbody #'(let-attributes (l ...) . body)])
       (if (syntax->datum #'syntax?)
           (case (syntax->datum #'depth)
             [(0) #'(with-syntax ((name       value))  bbody)]
             [(1) #'(with-syntax (((name ddd) value))  bbody)]
             [(2) #'(with-syntax ((((name ddd) ddd)
                                   value))  bbody)]
             [(3) #'(with-syntax (((((name ddd) ddd) ddd)
                                   value))  bbody)])
           #'(let ((name value)) bbody)
           #;#'(let-syntax ((name (nonsyntax-attribute-value 'name value)))
               bbody)))])))
#;   
(define-syntax let-attributes 
  (lambda (stx)
  (define (parse-attr x)
    (syntax-case (s-sharp x) (attr)
      [(attr name depth syntax?) #'(name depth syntax?)]))

  (syntax-case stx ()
    [(let-attributes ([a value] ...) . body)
     (with-syntax ([((name depth syntax?) ...)
                    (map parse-attr (stx->list #'(a ...)))])
       (with-syntax ([(vtmp ...) (generate-temporaries #'(name ...))]
                     [(stmp ...) (generate-temporaries #'(name ...))])         
         #'(let ([vtmp value] ...)
             (let-syntax ([stmp (make-attribute-mapping 
                                 #'vtmp 'name 'depth 'syntax?)] ...)
               (letrec-syntax
                   ([name (make-syntax-mapping 'depth #'stmp)] 
                    ...)                 
                 . body)))))])))

;; (let-attributes* (([id num] ...) (expr ...)) expr) : expr
;; Special case: empty attrs need not match number of value exprs.
(define-syntax let-attributes*
  (syntax-rules ()
    [(la* (() _) . body)
     (let () . body)]
    [(la* ((a ...) (val ...)) . body)
     (let-attributes ([a val] ...) . body)]))

;; (let/unpack (([id num] ...) expr) expr) : expr
;; Special case: empty attrs need not match packed length
(define-syntax let/unpack 
  (lambda (stx)
    (syntax-case stx ()
      [(let/unpack (() packed) body)
       #'body]
      [(let/unpack ((a ...) packed) body)
       (with-syntax ([(tmp ...) (generate-temporaries #'(a ...))])
         #'(let-values ([(tmp ...) (apply values packed)])
             (let-attributes ([a tmp] ...) body)))])))

(define-syntax attribute
  (lambda (stx)
    (syntax-case stx () 
      ((_ x)       
       (let-values (((k v) (syntax-local-binding #'x)))                  
         (case k
           ((macro)
            (if (attribute? v)
                (car (attribute? v))
                (raise-syntax-error 'attribute "not a attribute argument" stx)))
           ((pattern-variable)
            (with-syntax ((kk (cdr v)))
              #'(pattern-attr x kk)))
           ((lexical)
            #'x))))
      ((_ x k)
       (if (= (syntax->datum #'k) 0)
           #'(attribute x)
           #'(pattern-attr x k))))))

(define-syntax pattern-attr
  (lambda (stx)
    (syntax-case stx ()
      ((_ x k)
       (with-syntax ((ooo (datum->syntax #'x '...)))
	  (let-values (((key v) (syntax-local-binding #'x)))                  
	    (case key
	      ((macro)
	       (error "macro in (attribute x k) not supported")
	       (if (attribute? v)
		   (car (attribute? v))
		   (raise-syntax-error 
		    'attribute "not a attribute argument" stx)))
	      ((pattern-variable)
	       (case (syntax->datum #'k)
		 ((0) #'#'x)
		 ((1) #'#'(x ooo))
		 ((2) #'#'((x ooo) ooo))
		 ((3) #'#'(((x ooo) ooo) ooo))
		 ((4) #'#'((((x ooo) ooo) ooo) ooo))))

	      ((lexical)
	       #'x))))))))




;; (attribute-binding id)
;; mostly for debugging/testing
(define-syntax attribute-binding 
  (lambda (stx)
    (syntax-case stx ()
      [(attribute-bound? name)
       (identifier? #'name)
       (let ([value (syntax-local-value #'name)])
         (if (syntax-pattern-variable? value)
             (let ([value (syntax-local-value (syntax-mapping-valvar value))])
               (if (syntax-data-apply
                    attribute-mapping? value)
                   #`(quote #,(make-attr (syntax-data-apply
                                          attribute-mapping-name value)
                                         (syntax-data-apply
                                          attribute-mapping-depth value)
                                         (syntax-data-apply
                                          attribute-mapping-syntax? value)))
                   #'(quote #f)))
             #'(quote #f)))])))

;; (check-list^depth attr expr)
(define-syntax check-list^depth 
  (lambda (stx)
    (syntax-case stx ()
      [(_ a expr)
       (with-syntax ([(attr name depth syntax?) (s-sharp #'a)])
         (quasisyntax/loc #'expr
           (check-list^depth* 'name 'depth expr)))])))

(define (check-list^depth* aname n0 v0)
  (define (loop n v)
    (when (positive? n)
      (unless (list? v)
        (raise-type-error aname (format #f "lists nested ~a deep" n0) v))
      (let l ((v v))
        (if (pair? v)
            (loop (- n 1) (car v))))))            
  (loop n0 v0)
  v0)


;; ====

(export check-literal
        free-identifier=?/phases)

;; check-literal : id phase-level stx -> void
;; FIXME: change to normal 'error', if src gets stripped away
(define (check-literal id phase ctx)
  (unless (identifier-binding id phase)
    (raise-syntax-error #f
                        "literal is unbound"
                        ctx id)))

;; free-identifier=?/phases : id phase-level id phase-level -> boolean
;; Determines whether x has the same binding at phase-level phase-x
;; that y has at phase-level y.
;; At least one of the identifiers MUST have a binding (module or lexical)
    
(define (free-identifier=?/phases x phase-x y phase-y)
  (free-identifier=? x y))

#;
(define (free-identifier=?/phases x phase-x y phase-y)
  (let ([bx (identifier-binding x phase-x)]
        [by (identifier-binding y phase-y)])
    (cond [(and (list? bx) (list? by))
           (let ([modx (module-path-index-resolve (first bx))]
                 [namex (second bx)]
                 [phasex (fifth bx)]
                 [mody (module-path-index-resolve (first by))]
                 [namey (second by)]
                 [phasey (fifth by)])
             (and (eq? modx mody) ;; resolved-module-paths are interned
                  (eq? namex namey)
                  (equal? phasex phasey)))]
          [else
           ;; One must be lexical (can't be #f, since one must be bound)
           ;; lexically-bound names bound in only one phase; just compare
           (free-identifier=? x y)])))

;; ----

(export begin-for-syntax/once)

;; (begin-for-syntax/once expr/phase1 ...)
;; evaluates in pass 2 of module/intdefs expansion

#;
(define-syntax begin-for-syntax/once 
  (lambda (stx)
  (syntax-case stx ()
    [(bfs/o e ...)
     (cond [(list? (syntax-local-context))
            #`(define-values ()
                (begin (begin-for-syntax/once e ...)
                       (values)))]
           [else
            #'(let-syntax ([m (lambda _ (begin e ...) #'(void))])
                (m))])])))

(define-syntax begin-for-syntax/once 
  (lambda (stx)
  (syntax-case stx ()
    [(bfs/o e ...)
     #'(let-syntax ([m (lambda _ (begin e ...) #'(void))])
         (m))])))

;; ====

;; we do not do anything for this
(export no-shadow)

#;
(define (check-shadow def)
  (syntax-case def ()
    [(_def (x ...) . _)
     (with-fluids ((current-syntax-context def))
        (let loop ((l (stx->list #'(x ...))))
          (let ((v (syntax-local-value x)))
            (when (syntax-pattern-variable? v)
              (error
               "definition in pattern must not shadow attribute binding"
               )))))]))


(define-syntax no-shadow 
  (lambda (stx)
    (syntax-case stx () [(_ e) #'e]))
#;
  (lambda (stx)
    (syntax-case stx ()
      [(no-shadow e)
       (let ([ee (local-expand #'e (syntax-local-context)
                               (kernel-form-identifier-list))])
         (syntax-case ee (begin define-values define-syntaxes)
           [(begin d ...)
            #'(begin (no-shadow d) ...)]
           [(define-values . _)
            (begin (check-shadow ee)
                   ee)]
           [(define-syntaxes . _)
            (begin (check-shadow ee)
                   ee)]
           [_
  ee]))])))

;; ----

(export curried-stxclass-parser
        app-argu)

(define-syntax curried-stxclass-parser 
  (lambda (stx)
    (syntax-case stx ()
      [(_ class argu)
       (with-syntax ([(arguments (parg ...) (kw ...) _) (s-sharp #'argu)])
         (let ([sc (get-stxclass/check-arity #'class #'class
                         (length (stx->list #'(parg ...)))
                         (syntax->datum #'(kw ...)))])
         (with-syntax ([parser (stxclass-parser sc)])
           #'(lambda (x cx pr es fh cp success)
               (app-argu parser x cx pr es fh cp success argu)))))])))

(define-syntax app-argu 
  (lambda (stx)
    (syntax-case stx ()
      [(aa proc extra-parg ... ag )
       (syntax-case (s-sharp #'ag) (arguments)
         [(_ (parg ...) (kw ...) (kwarg ...))
       #|
       Use keyword-apply directly?
       #'(keyword-apply proc '(kw ...) (list kwarg ...) parg ... null)
       If so, create separate no-keyword clause.
       |#
     ;; For now, let #%app handle it.
       (with-syntax ([((kw-part ...) ...) #'((kw kwarg) ...)])
         #'(proc extra-parg ... parg ... kw-part ... ...))])])))

