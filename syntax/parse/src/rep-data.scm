;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src rep-data)
  #:use-module (syntax parse src kws)
  #:use-module (syntax parse src rep-attrs)
  #:use-module (syntax parse src rep-patterns)
  #:use-module (compat racket misc)

  #:use-module (syntax id-table)
  
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-11)

  #:export (stxclass/s?
            stxclass/h?
            stxclass-commit?
            stxclass-delimit-cut?))
            

(re-export-all (syntax parse src rep-attrs))
(re-export-all (syntax parse src rep-patterns))


#|
#lang racket/base
(require racket/contract/base
         racket/dict
         racket/list
         syntax/id-table
         unstable/syntax
         "minimatch.rkt"
         "kws.rkt"
         "rep-attrs.rkt"
         "rep-patterns.rkt")
(provide (all-from-out "rep-attrs.rkt")
         (all-from-out "rep-patterns.rkt")
         (struct-out stxclass)
         (struct-out options)
         (struct-out integrate)
         stxclass/s?
         stxclass/h?
         stxclass-commit?
         stxclass-delimit-cut?
         (struct-out attr)
         (struct-out rhs)
         (struct-out variant)
         (struct-out clause:fail)
         (struct-out clause:with)
         (struct-out clause:attr)
         (struct-out clause:do)
         (struct-out conventions)
         (struct-out literalset)
         (struct-out eh-alternative-set)
         (struct-out eh-alternative))

|#

#|
A stxclass is
  #s(stxclass symbol (listof symbol) (list-of SAttr) identifier bool Options Integrate/#f)
where Options = #s(options boolean boolean)
      Integrate = #s(integrate id string)
Arity is defined in kws.rkt
|#
(define-struct stxclass (name arity attrs parser splicing? options integrate))

(define-struct options (commit? delimit-cut?))
(define-struct integrate (predicate description))

(define (stxclass/s? x)  
  (and (stxclass? x) (not (stxclass-splicing? x))))
(define (stxclass/h? x)
  (and (stxclass? x) (stxclass-splicing? x)))

(define (stxclass-commit? x)
  (options-commit? (stxclass-options x)))
(define (stxclass-delimit-cut? x)
  (options-delimit-cut? (stxclass-options x)))

#|
An RHS is
  #s(rhs stx (listof SAttr) bool stx/#f (listof Variant) (listof stx) Options Integrate/#f)
definitions: auxiliary definitions from #:declare
|#
(define-struct rhs (ostx attrs transparent? description variants definitions options integrate))

#|
A Variant is
  (make-variant stx (listof SAttr) Pattern (listof stx))
|#
(define-struct variant (ostx attrs pattern definitions))

#|
SideClause is defined in rep-patterns
|#

#|
A Conventions is
  (make-conventions id (-> (listof ConventionRule)))
A ConventionRule is (list regexp DeclEntry)
|#
(define-struct conventions (get-procedures get-rules))

#|
A LiteralSet is
  (make-literalset (listof (list symbol id)) stx)
|#
(define-struct literalset (literals phase))

;; make-dummy-stxclass : identifier -> SC
;; Dummy stxclass for calculating attributes of recursive stxclasses.
(define (make-dummy-stxclass name)
  (make stxclass (syntax-e name) #f null #f #f (make options #f #t) #f))

#|
An EH-alternative-set is
  (eh-alternative-set (listof EH-alternative)
An EH-alternative is
  (eh-alternative RepetitionConstraint (listof SAttr) id)
|#
(define-struct eh-alternative-set (alts))
(define-struct eh-alternative (repc attrs parser))

;; Environments

#|
DeclEnv =
  (make-declenv immutable-bound-id-mapping[id => DeclEntry]
                (listof ConventionRule))

DeclEntry =
  (den:lit id id ct-phase ct-phase)
  (den:class id id Arguments)
  (den:parser id (listof SAttr) bool bool bool)
  (den:delayed id id)

Arguments is defined in rep-patterns.rkt
|#
(define-struct declenv (table conventions))

(define-struct den:lit     (internal external input-phase lit-phase))
(define-struct den:class   (name class argu))
(define-struct den:parser  (parser attrs splicing? commit? delimit-cut?))
(define-struct den:delayed (parser class))

(define* (new-declenv literals #:key [conventions null])
  (make-declenv
   (let loop ([table    (make-bound-id-table)]
              [literals literals])
     (if (pair? literals)
         (let ((literal (car literals)))
           (loop
            (bound-id-table-set! table (car literal)
                                 (make den:lit (first literal) (second literal)
                                       (third literal) (fourth literal)))
            (cdr literals)))
         table))
   conventions))

(define* (declenv-lookup env id #:key [use-conventions? #t])
  (or (bound-id-table-ref (declenv-table env) id #f)
      (and use-conventions?
           (conventions-lookup (declenv-conventions env) id))))

(define* (declenv-check-unbound env id #:optional [stxclass-name #f]
                                #:key [blame-declare? #f])
  ;; Order goes: literals, pattern, declares
  ;; So blame-declare? only applies to stxclass declares
  (let ([val (declenv-lookup env id #:use-conventions? #f)])
    (match val
      [((? den:lit) _i _e _ip _lp)
       (wrong-syntax id "identifier previously declared as literal")]
      [((? den:class) name _c _a)
       (if (and blame-declare? stxclass-name)
           (wrong-syntax name
                         "identifier previously declared with syntax class ~a"
                         stxclass-name)
           (wrong-syntax (if blame-declare? name id)
                         "identifier previously declared"))]
      [((? den:parser) _p _a _sp _c _dc?)
       (wrong-syntax id "(internal error) late unbound check")]
      [#f (void)])))

(define (declenv-put-stxclass env id stxclass-name argu)
  (declenv-check-unbound env id)
  (make-declenv
   (bound-id-table-set! (declenv-table env) id
                        (make den:class id stxclass-name argu))
   (declenv-conventions env)))

;; declenv-update/fold : DeclEnv (Id/Regexp DeclEntry a -> DeclEntry a) a
;;                    -> (values DeclEnv a)
(define (declenv-update/fold env0 f acc0)
  (define-values (acc1 rules1)
    (let loop ([acc      acc0]
               [newrules null]
               [rules    (declenv-conventions env0)])
      (if (pair? rules)
          (let ((rule (car rules)))
            (let-values ([(val acc) (f (car rule) (cadr rule) acc)])
              (loop acc (cons (list (car rule) val) newrules) 
                    (cdr rules))))
          (values acc newrules))))

  (define-values (acc2 table2)
    (let ((tb (declenv-table env0)))
      (let loop ([acc   acc1] 
                 [table (make-bound-id-table)]
                 [ks    (bound-id-table-map (lambda (k v) k) tb)]
                 [vs    (bound-id-table-map (lambda (k v) v) tb)])
        (if (pair? ks)
            (let ((k (car ks)) (v (car vs)))
              (let-values ([(val acc) (f k v acc)])
                (loop acc
                      (bound-id-table-set! table k val)
                      (cdr ks)
                      (cdr vs))))
            (values acc table)))))

  (values (make-declenv table2 (reverse rules1))
          acc2))

;; returns ids in domain of env but not in given list
(define (declenv-domain-difference env ids)
  (define idbm (make-bound-id-table))
  (for-each (lambda (id)
              (bound-id-table-set! idbm id #t))
            ids)
  (let loop ((ss (hash-map->list (lambda (k s) (cons k s))  
                                 (declenv-table env))))
    (if (pair? ss)
        (let ((k (caar ss))
              (v (cdar ss)))
          (if (and (or (den:class? v) (den:parser? v))
                   (not (bound-id-table-ref idbm k #f)))
              (cons k (loop (cdr ss)))
              (loop (cdr ss))))
        '())))

;; Conventions = (listof (list regexp DeclEntry))

(define (conventions-lookup conventions id)
  (let ([sym (symbol->string (syntax-e id))])
    (ormap (lambda (c) (and (regexp-exec (car c)  sym) (cadr c)))
           conventions)))


;; Contracts
#;
(define DeclEnv/c
  (flat-named-contract 'DeclEnv declenv?))

#;
(define DeclEntry/c
  (flat-named-contract 'DeclEntry
                       (or/c den:lit? den:class? den:parser? den:delayed?)))

#;
(define SideClause/c
  (or/c clause:fail? clause:with? clause:attr? clause:do?))

;; ct-phase = syntax, expr that computes absolute phase
;;   usually = #'(syntax-local-phase-level)
(define ct-phase/c syntax?)

(struct-out den:lit)
(struct-out den:class)
(struct-out den:parser)
(struct-out den:delayed)

(export
 make-dummy-stxclass
 stxclass-lookup-config
 new-declenv
 declenv-lookup
 declenv-put-stxclass
 declenv-domain-difference
 declenv-update/fold
 get-stxclass
 get-stxclass/check-arity
 split-id/get-stxclass)

;; stxclass-lookup-config : (parameterof (U 'no 'try 'yes))
;;  'no means don't lookup, always use dummy (no nested attrs)
;;  'try means lookup, but on failure use dummy (-> nested attrs only from prev.)
;;  'yes means lookup, raise error on failure

(define stxclass-lookup-config (make-fluid))
(fluid-set! stxclass-lookup-config 'yes)


(define (get-stxclass id)
  (define config (fluid-ref stxclass-lookup-config))
  (define (stx-local? id)
    (aif (it) (syntax-local-value id)
         (aif (it2) (is-class? it)
              it2
              #f)
         #f))

  (if (eq? config 'no)
      (make-dummy-stxclass id)
      (cond [(stx-local? id) => values]
            [(eq? config 'try)
             (make-dummy-stxclass id)]
            [else (wrong-syntax id "not defined as syntax class")])))

(define (get-stxclass/check-arity id stx pos-count keywords)
  (let ([sc (get-stxclass id)])
    (unless (memq (fluid-ref stxclass-lookup-config) '(try no))
      (check-arity (stxclass-arity sc) pos-count keywords
                   (lambda (msg)
                     (raise-syntax-error #f msg stx))))
    sc))

(define (split-id/get-stxclass id0 decls)
  (define (f x)
    (if (null? x)
        #f
        x))
  (cond [(f (list-matches "^([^:]*):(.+)$"
                          (symbol->string (syntax-e id0))))
         => (lambda (m)
              (let ((m (car m)))
                (define id
                  (datum->syntax id0 (string->symbol (match:substring m 1))))
                (define scname
                  (datum->syntax id0 (string->symbol (match:substring m 2))))
                (declenv-check-unbound decls id (syntax-e scname)
                                       #:blame-declare? #t)
                (let ([sc (get-stxclass/check-arity scname id0 0 null)])
                  (values id sc))))]
        [else (values id0 #f)]))

;; ----

(export get-eh-alternative-set)

(define (get-eh-alternative-set id)
  (let ([v (syntax-data-apply (lambda (x) x) (syntax-local-value id))])
    (unless (eh-alternative-set? v)
      (wrong-syntax id "not defined as an eh-alternative-set"))
    v))


(struct-out stxclass)
(struct-out options)
(struct-out integrate)
(re-struct-out attr)
(struct-out rhs)
(struct-out variant)
(struct-out conventions)
(struct-out literalset)
(struct-out eh-alternative-set)
(struct-out eh-alternative)


