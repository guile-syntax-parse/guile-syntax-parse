;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src runtime-report)
  #:use-module (syntax parse src runtime)
  #:use-module (syntax parse src kws)
  #:use-module (compat racket misc)

  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (ice-9 match)

  #:export (syntax-patterns-fail
            current-failure-handler))
#|
#lang racket/base
(require racket/list
         "minimatch.rkt"
         "runtime.rkt"
         "kws.rkt")
(provide syntax-patterns-fail
         current-failure-handler)

|#

(define (syntax-patterns-fail stx0) 
  (lambda (fs)
    (call-with-values (lambda () ((fluid-ref current-failure-handler) stx0 fs))
      (lambda vals
        (rerror 'current-failure-handler
               "current-failure-handler: did not escape, produced ~a"
               (case (length vals)
                 ((1) (car vals))
                 (else (cons 'values vals))))))))

(define (default-failure-handler stx0 fs)
  (report-failureset stx0 fs))

(define current-failure-handler
  (make-fluid default-failure-handler))

;; ----

#|
Reporting
---------

First, failures with maximal (normalized) progresses are selected and
grouped into equivalence classes. In principle, each failure in an
equivalence class complains about the same term, but in practice,
special handling of failures like "unexpected term" make things more
complicated.

|#

;; report-failureset : stx FailureSet -> escapes
(define (report-failureset stx0 fs)
  (let* ([classes (maximal-failures fs)]
         [reports (apply append (map report/class classes))])
    (raise-syntax-error/reports stx0 reports)))

;; A Report is
;;   - (report string stx)
;(define-struct report (message stx) #:prefab)
(define-record-type report
  (make-report message stx)
  report?
  (message report-message)
  (stx     report-stx))

;; report/class : (non-empty-listof Failure) -> (listof Report)
(define (report/class fs)
  (let* ([ess (map failure-expectstack fs)]
         [ess (map normalize-expectstack ess)]
         [ess (remove-duplicates ess)]
         [ess (simplify-common-expectstacks ess)])
    (let-values ([(stx index) (ps->stx+index (failure-progress (car fs)))])
      (let loop ((ess ess))
        (if (pair? ess)
            (cons
             (report/expectstack (car ess) stx index)
             (loop (cdr ess)))
            '())))))

;; report/expectstack : ExpectStack syntax nat -> Report
(define (report/expectstack es stx index)
  (let ([frame-expect (let ((r (and (pair? es) (car es))))
                        (match r
                          (($ expect:message #f)
                           (if (pair? (cdr es))
                               (cadr es)
                               r))
                          (r r)))])
    (cond [(not frame-expect)
           (make-report "bad syntax" #f)]
          [else
           (let ([frame-stx
                  (let-values ([(x cx) (stx-list-drop/cx stx stx index)])
                    (let ((cx (let loop ((cx cx))
                                (if (pair? cx) 
                                    (loop (car cx))
                                    cx))))
                      (rdatum->syntax cx x)))])
             (cond [(equal? frame-expect (make-expect:atom '()))
                    (syntax-case frame-stx ()
                      [(one . more)
                       (make report "unexpected term" #'one)]
                      [_
                       (report/expects (list frame-expect) frame-stx)])]
                   [(expect:disj? frame-expect)
                    (report/expects (expect:disj-expects frame-expect) 
                                    frame-stx)]
                   [else
                    (report/expects (list frame-expect) frame-stx)]))])))

;; report/expects : (listof Expect) syntax -> Report
(define (report/expects expects frame-stx)
  (make report (join-sep (let loop ((expects expects))
                      (if (pair? expects)
                          (cons
                           (prose-for-expect (car expects))
                           (loop (cdr expects)))
                          '()))
                    ";" "or")
          frame-stx))

;; prose-for-expect : Expect -> string
(define (prose-for-expect e)
  (match e
    [($ expect:thing description transparent?)
     (format #f "expected ~a" description)]
    [($ expect:atom atom)
     (format #f "expected the literal ~a~s~a"
             (if (symbol? atom) "symbol `" "")
             atom
             (if (symbol? atom) "'" ""))]
    [($ expect:literal literal)
     (format #f "expected the identifier `~s'" (syntax-e literal))]
    [($ expect:message message)
     (format #f "~a" message)]))

;; == Do Report ==

(define (raise-syntax-error/reports stx0 reports)
  (cond [(= (length reports) 1)
         (raise-syntax-error/report stx0 (car reports))]
        [else
         (raise-syntax-error/report* stx0 (car reports))]))

(define (raise-syntax-error/report stx0 report)
  (raise-syntax-error #f (report-message report) stx0 (report-stx report)))

(define (raise-syntax-error/report* stx0 report)
  (let ([message
         (u-string-append
          "There were multiple syntax errors. The first error follows:\n"
          (report-message report))])
    (raise-syntax-error #f message stx0 (report-stx report))))

;; ====

(define (comma-list items)
  (join-sep items "," "or"))

(define (improper-stx->list stx)
  (syntax-case stx ()
    [(a . b) (cons #'a (improper-stx->list #'b))]
    [() null]
    [rest (list #'rest)]))
