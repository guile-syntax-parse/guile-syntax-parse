;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src runtime-reflect)
  #:use-module (syntax parse src rep-data)
  #:use-module (syntax parse src rep-attrs)
  #:use-module (syntax parse src kws)
  #:use-module (compat racket misc)
  #:use-module (rnrs records syntactic)

  #:export (reified-base-name
            
            reified
            reified?
            make-reified

            reified-name
            reified-parser
            reified-arity
            reified-signature

            reified-syntax-clause
            make-reified-syntax-clause
            reified-syntax-clause?
            
            reified-splicing-syntax-class
            reified-splicing-syntax-class?
            make-reified-splicing-syntax-class
            
            reflect-parser))
            
#|
lang racket/base
(require (for-syntax racket/base
                     "rep-data.rkt")
         "rep-attrs.rkt"
         "kws.rkt")
(provide (struct-out reified)
         (struct-out reified-syntax-class)
         (struct-out reified-splicing-syntax-class)
         reify-syntax-class
         reified-syntax-class?
         reified-splicing-syntax-class?
         reflect-parser)
|#

#|
A Reified is
  (reified symbol ParserFunction nat (listof (list symbol nat)))
|#

;(define-struct reified-base (name) #:transparent)
(define-record-type 
  (reified-base make-reified-base reified-base?)
  (fields (immutable name reified-base-name)))

;(define-struct (reified reified-base) (parser arity signature))
(define-record-type 
  (reified make-reified reified?)
  (parent reified-base)
  (fields
   (immutable parser    reified-parser)
   (immutable arity     reified-arity)
   (immutable signature reified-signature)))

;(define-struct (reified-syntax-class reified) ())
(define-record-type
  (reified-syntax-class make-reified-syntax-class reified-syntax-class?)
  (parent reified))

;(define-struct (reified-splicing-syntax-class reified) ())
(define-record-type
  (reified-splicing-syntax-class make-reified-splicing-syntax-class 
                                 reified-splicing-syntax-class?)
  (parent reified))
   
;; ----
(define reified-name reified-base-name)

(define-syntax reify-syntax-class 
  (lambda (stx)
    (syntax-case stx ()
      [(rsc sc)
       (let* ([stxclass  (get-stxclass #'sc)]
              [splicing? (stxclass-splicing? stxclass)])
         (unless (stxclass-delimit-cut? stxclass)
           (raise-syntax-error 
            #f "cannot reify syntax class with #:no-delimit-cut option"
            stx #'sc))

         (with-syntax ([name (stxclass-name stxclass)]
                       [parser (stxclass-parser stxclass)]
                       [arity (stxclass-arity stxclass)]
                       [((attr aname adepth _) ...) 
                        (map s-sharp (stx->list (stxclass-attrs stxclass)))]
                       [ctor
                        (if splicing?
                            #'make-reified-splicing-syntax-class
                            #'make-reified-syntax-class)])
           #'(ctor 'name parser 'arity '((aname adepth) ...))))])))

;; e-arity represents single call; min and max are same
(define (reflect-parser obj e-arity e-attrs splicing?)
  (define who (if splicing? 
                  'reflect-splicing-syntax-class 
                  'reflect-syntax-class))

  (if splicing?
      (unless (reified-splicing-syntax-class? obj)
        (raise-type-error who "reified splicing-syntax-class" obj))
      (unless (reified-syntax-class? obj)
        (raise-type-error who "reified syntax-class" obj)))

  (check-params who e-arity (reified-arity obj) obj)
  (adapt-parser who
                (let loop ((e-attrs e-attrs))
                  (if (pair? e-attrs)
                      (let ((a (car e-attrs)))
                        (cons
                         (list (attr-name a) (attr-depth a))
                         (loop (cdr e-attrs))))
                      '()))

                (reified-signature obj)
                (reified-parser obj)
                splicing?))

(define (check-params who e-arity r-arity obj)
  (let ([e-pos (arity-minpos e-arity)]
        [e-kws (arity-minkws e-arity)])
    (check-arity/neg r-arity e-pos e-kws
                     (lambda (msg)
                       (raise-mismatch-error who (string-append msg ": ") 
                                             obj)))))

(define (adapt-parser who esig0 rsig0 parser splicing?)
  (if (equal? esig0 rsig0)
      parser
      (let ([indexes
             (let loop ([esig esig0] [rsig rsig0] [index 0])
               (cond [(null? esig)
                      null]
                     [(and (pair? rsig) (eq? (caar esig) (caar rsig)))
                      (unless (= (cadar esig) (cadar rsig))
                        (wrong-depth who (car esig) (car rsig)))
                      (cons index (loop (cdr esig) (cdr rsig) (add1 index)))]
                     [(and (pair? rsig)
                           (string>? (symbol->string (caar esig))
                                     (symbol->string (caar rsig))))
                      (loop esig (cdr rsig) (add1 index))]
                     [else
                      (rerror 
                       who 
                       "reified syntax-class is missing declared attribute `~s'"
                       (caar esig))]))])
        (define (take-indexes result indexes)
          (let loop ([result result] [indexes indexes] [i 0])
            (cond [(null? indexes) null]
                  [(= (car indexes) i)
                   (cons (car result) (loop (cdr result) (cdr indexes) 
					    (add1 i)))]
                  [else
                   (loop (cdr result) indexes (add1 i))])))
        (make-keyword-procedure
         (lambda (kws kwargs x cx pr es fh cp success . rest)
           (keyword-apply parser kws kwargs x cx pr es fh cp
                          (if splicing?
                              (lambda (fh cp x cx . result)
                                (apply success fh cp x cx 
				       (take-indexes result indexes)))
                              (lambda (fh cp . result)
                                (apply success fh cp 
				       (take-indexes result indexes))))
                          rest))))))

(define (wrong-depth who a b)
  (rerror who
         "reified syntax-class has wrong depth for attribute `~s'; expected ~s, got ~s instead"
         (car a) (cadr a) (cadr b)))
