;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src sc)
  #:use-module (syntax parse src parse)
  #:use-module (syntax parse src keywords)
  #:use-module (syntax parse src runtime)
  #:use-module (syntax parse src rep-attrs)
  #:use-module (syntax parse src rep-data)
  #:use-module (syntax parse src rep)
  #:use-module (syntax parse src kws)
  #:use-module (compat racket misc)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 pretty-print)
  
  #:re-export (attribute this-syntax)
  #:export ( define-syntax-class
	     syntax-class-set!
             define-splicing-syntax-class
	     splicing-syntax-class-set!
             with-syntax-class
             with-syntax-class-splicing
             syntax-parse
             syntax-parser
             
             ;;----
             parser/rhs))



(if (not (ver)) 
    (error "syntax-parse is only supported for guile 2.0 or higher"))

(re-export-all (syntax parse src keywords)
               #:except (~reflect ~splicing-reflect ~eh-var))
(define (ppp x)
  (pretty-print (syntax->datum x))
  x)
#|  
#lang racket/base
(require (for-syntax racket/base
                     syntax/stx
                     unstable/syntax
                     "rep-data.rkt"
                     "rep.rkt")
         "parse.rkt"
         "keywords.rkt"
         "runtime.rkt")

(provide define-syntax-class
         define-splicing-syntax-class
         
         syntax-parse
         syntax-parser

         (except-out (all-from-out "keywords.rkt")
                     ~reflect
                     ~splicing-reflect
                     ~eh-var)

         attribute
         this-syntax

         ;;----
         parser/rhs)
|#

(define sc 'sc)
(define sc-stx #'sc)

(define (m stx acc it)
  (datum->syntax stx (acc it)))

(define (test-stx-cls a b)
  (if (equal? a b)
      #t
      (begin
	(format #t "fail-> new: ~a  !=  old: ~a~%" a b)
	#f)))

(define-syntax mkclass
  (lambda (x)
    (syntax-case x ()
      ((_ #f name arity (attrs ...) parser splicing options integrate)
       #'(define-syntax name 
	   (class-in-lambda
	    (make-stxclass 'name 'arity
			   (list 'attrs ...)
                           (case (ver)
                             ((v2.0) (datum->syntax #'name 'parser))
                             ((v2.1 v2.2 v2.3 v2.4 v2.9 v3.0) #'parser))
			   'splicing
			   'options
			   integrate))))
	
      ((_ #t name arity (attrs ...) parser splicing options integrate)
       (let ((default 
	       #'(begin 
		   (warn "defines a new syntax class with a set!")
		   (define-syntax name 
		     (class-in-lambda
		      (make-stxclass 'name 'arity
				     (list 'attrs ...)
                                     (datum->syntax #'name 'parser)
				     (case (ver)
                                       ((v2.0) (datum->syntax #'name 'parser))
                                       ((v2.1 v2.2 v2.3 v2.4 v2.9 v3.0)
                                        #'parser))
				     'splicing
				     'options
				     integrate))))))
			    
	 (aif (it) (syntax-local-value #'name)
	      (aif (it) (is-class? it)
		   #`(if (and 
			  (test-stx-cls 'arity 
					'#,(m #'name stxclass-arity it))
			  (test-stx-cls (list 'attrs ...)
					'#,(m #'name stxclass-attrs it))
			  (test-stx-cls splicing #,(m #'name stxclass-splicing? 
						      it))
			  (test-stx-cls 'options '#,(m #'name stxclass-options 
						       it))
			  (test-stx-cls integrate #,(m #'name 
						       stxclass-integrate 
						       it)))

			 (set! #,(stxclass-parser it) 
			       parser)
			 (error "could not set a new version of the syntax"))
		   default)
	      default))))))
		 

  
(define (defstxclass stx name formals rhss splicing? set?)
  (with-fluids ((current-syntax-context stx))
    (with-syntax ([name    name]
                  [formals formals]
                  [rhss    rhss])
      (let* ([the-rhs (parse-rhs #'rhss #f splicing? #:context stx)]
             [arity   (parse-kw-formals #'formals #:context stx)]
             [att     (rhs-attrs the-rhs)]
             [opt-rhs+def
               (and (stx-list? #'formals) 
                    (andmap identifier? (syntax->list #'formals))
                    (optimize-rhs the-rhs (syntax->list #'formals)))]
             [the-rhs (if opt-rhs+def (car opt-rhs+def) the-rhs)])
        (with-syntax ([parser (generate-temporary
                               (format-symbol "parse-~a" 
                                              (syntax-e #'name)))]
		      [set?    set?]
                      [arity   arity]
                      [(attrs ...)   att]
                      [(opt-def ...)
                       (if opt-rhs+def
                           (list (cadr opt-rhs+def))
                           '())]
                      [options (rhs-options the-rhs)]
                       [integrate-expr
                        (syntax-case (s-sharp (rhs-integrate the-rhs))
                            (integrate)
                          [(integrate predicate description)
                           #'(make-integrate #'predicate
                                             'description)]
                          [#f
                           #''#f])])          
          #`(begin               
                 opt-def ...
                 (define-values (parser)
                   ;; If opt-rhs, do not reparse:
                   ;; need to keep same generated predicate name
                   #,(if opt-rhs+def
                         (begin
                           #;(printf "Integrable syntax class: ~s\n" 
                           (syntax->datum #'name))
                           #`(parser/rhs/parsed
                              name formals (attrs ...) #,the-rhs
                              #,(and (rhs-description the-rhs) #t)
                              #,splicing? #,stx))
                         #`(parser/rhs
                            name formals (attrs ...) rhss 
                            #,splicing? #,stx)))
		 
                 (mkclass set? name arity
			  (attrs ...)
			  parser
			  #,splicing?
			  options
			  integrate-expr)))))))

(define (withstxclass stx name formals rhss splicing? code-in)
  (with-fluids ((current-syntax-context stx))
    (with-syntax ([name    name]
                  [formals formals]
                  [rhss    rhss])
      (let* ([the-rhs (parse-rhs #'rhss #f splicing? #:context stx)]
             [arity   (parse-kw-formals #'formals #:context stx)]
             [att     (rhs-attrs the-rhs)]
             [opt-rhs+def
               (and (stx-list? #'formals) 
                    (andmap identifier? (syntax->list #'formals))
                    (optimize-rhs the-rhs (syntax->list #'formals)))]
             [the-rhs (if opt-rhs+def (car opt-rhs+def) the-rhs)])
        (with-syntax ([parser (generate-temporary
                               (format-symbol "parse-~a" 
                                              (syntax-e #'name)))]
                      [code    code-in]
                      [arity   arity]
                      [(attrs ...)   att]
                      [(opt-def ...)
                       (if opt-rhs+def
                           (list (cadr opt-rhs+def))
                           '())]
                      [options (rhs-options the-rhs)]
                       [integrate-expr
                        (syntax-case (s-sharp (rhs-integrate the-rhs)) 
                            (integrate)
                          [(integrate predicate description)
                           #'(make-integrate #'predicate
                                             'description)]
                          [#f
                           #''#f])])          
            #`(begin               
                 opt-def ...
                 (define-values (parser)
                   ;; If opt-rhs, do not reparse:
                   ;; need to keep same generated predicate name
                   #,(if opt-rhs+def
                         (begin
                           #;(printf "Integrable syntax class: ~s\n" 
                           (syntax->datum #'name))
                           #`(parser/rhs/parsed
                              name formals (attrs ...) #,the-rhs
                              #,(and (rhs-description the-rhs) #t)
                              #,splicing? #,stx))
                         #`(parser/rhs
                            name formals (attrs ...) rhss 
                            #,splicing? #,stx)))
                 (let ()
                   (define-syntax name
                     (class-in-lambda 
                      (make-stxclass 'name 'arity
                                     (list 'attrs ...)
                                     #'parser
                                     '#,splicing?
                                     'options
                                     integrate-expr)))
                   code)))))))


(define-syntax define-syntax-class 
  (lambda (stx)
    (syntax-case stx ()
    [(define-syntax-class (name . formals) . rhss)
     (identifier? #'name)
     (check-out (defstxclass stx #'name #'formals #'rhss #f #f))]

    [(define-syntax-class name . rhss)
     (identifier? #'name)
     (check-out (defstxclass stx #'name #'() #'rhss #f #f))])))

(define-syntax syntax-class-set!
  (lambda (stx)
    (syntax-case stx ()
    [(define-syntax-class (name . formals) . rhss)
     (identifier? #'name)
     (check-out (defstxclass stx #'name #'formals #'rhss #f #t))]

    [(define-syntax-class name . rhss)
     (identifier? #'name)
     (check-out (defstxclass stx #'name #'() #'rhss #f #t))])))

(define (check-out x)
  (let loop ((x x))
    (syntax-case x (make-attr)
      ((make-attr    . l) #t)
      ((make-options . l) #t)
      (_
       (if (or (syntax? x) (stx-pair? x) (stx-null? x) (not (symbol? x)))
           (if (stx-pair? x)
               (begin
                 (loop (stx-car x))
                 (loop (stx-cdr x)))
               #t)
           (error (format #f "this should be a syntax object: ~a" x))))))
  x)

(define-syntax define-splicing-syntax-class 
  (lambda (stx)
  (syntax-case stx ()
    [(define-splicing-syntax-class name . rhss)
     (identifier? #'name)
     (defstxclass stx #'name #'() #'rhss #t #f)]
    [(define-splicing-syntax-class (name . formals) . rhss)
     (identifier? #'name)
     (defstxclass stx #'name #'formals #'rhss #t #f)])))

(define-syntax splicing-syntax-class-set!
  (lambda (stx)
  (syntax-case stx ()
    [(define-splicing-syntax-class name . rhss)
     (identifier? #'name)
     (defstxclass stx #'name #'() #'rhss #t #t)]
    [(define-splicing-syntax-class (name . formals) . rhss)
     (identifier? #'name)
     (defstxclass stx #'name #'formals #'rhss #t #t)])))

(define-syntax with-syntax-class
  (lambda (stx)
    (syntax-case stx ()
      ((_ () code ...)
       #'(begin code ...))

      ((_ (((name . formals) . rhss) . l) . code)
       (identifier? #'name)
       (withstxclass stx #'name #'formals #'rhss #f
                     #'(with-syntax-class l . code)))

      ((_ ((name . rhss) . l) . code)
       (identifier? #'name)
       (withstxclass stx #'name #'()      #'rhss #f
                     #'(with-syntax-class l . code))))))

(define-syntax with-syntax-class-splicing
  (lambda (stx)
    (syntax-case stx ()
      ((_ () code ...)
       #'(begin code ...))

      ((_ ((name . rhss) . l) . code)
       (identifier? #'name)
       (withstxclass stx #'name #'()      #'rhss #t 
                     #'(with-syntax-class-splicing l . code)))

      ((_ (((name . formals) . rhss) . l) . code)
       (identifier? #'name)
       (withstxclass stx #'name #'formals #'rhss #t
                     #'(with-syntax-class-splicing l . code))))))
      
;; ----

(define-syntax parser/rhs 
  (lambda (stx)
    (pkk 'parser/rhs/)
  (syntax-case stx ()
    [(parser/rhs name formals attrs rhss splicing? ctx)
     (let ([rhs
            (with-fluids ((current-syntax-context #'ctx))
               (parse-rhs #'rhss (syntax->datum #'attrs) (syntax-e #'splicing?)
                          #:context #'ctx))])
       #`(parser/rhs/parsed name formals attrs
                            #,rhs #,(and (rhs-description rhs) #t)
                            splicing? ctx))])))

(define-syntax parser/rhs/parsed 
  (lambda (stx)
  (pkk 'parser/rhs/parsed)
  (syntax-case stx ()
    [(prp name formals attrs rhs rhs-has-description? splicing? ctx)
     (begin
       #`(let ([get-description 
              (rlambda formals
                (if rhs-has-description?
                    #,(rhs-description (syntax->list #'rhs))
                    (symbol->string 'name)))])         
         (parse:rhs rhs attrs formals splicing?
                    (if rhs-has-description?
                        #,(rhs-description (syntax->list #'rhs))
                        (symbol->string 'name)))))])))

;; ====
(define-syntax syntax-parse 
  (lambda (stx)
    (syntax-case stx ()
      [(syntax-parse stx-expr . clauses)     
       (quasisyntax/loc stx
         (let ([x stx-expr])
           (parse:clauses x clauses #,stx)))])))

(define-syntax syntax-parser 
  (lambda (stx)
    (syntax-case stx ()
      [(syntax-parser . clauses)
       (quasisyntax/loc stx
           (lambda (x)
             (parse:clauses x clauses #,stx)))])))

