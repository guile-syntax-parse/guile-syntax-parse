;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src litconv)
  #:use-module (compat racket misc)
  #:use-module (syntax parse src sc)
  #:use-module (syntax parse src lib)
  #:use-module (syntax parse src rep-data)
  #:use-module (syntax parse src rep)
  #:use-module (syntax parse src kws)
  #:use-module (syntax parse src runtime)

  #:use-module (srfi srfi-11)

  #:export ( define-conventions
             with-syntax-conventions
             define-literal-set
             with-syntax-literal-set
             #;kernel-literals))

(define-syntax-rule (rlambda . l) (lambda . l))

#|
#lang racket/base
(require (for-syntax racket/base
                     "sc.rkt"
                     "lib.rkt"
                     unstable/syntax
                     "rep-data.rkt"
                     "rep.rkt"
                     "kws.rkt")
         "runtime.rkt")
(provide define-conventions
         define-literal-set
         kernel-literals)
|#


(define-syntax define-conventions 
  (lambda (stx)
    (with-syntax-class
     ((header       
        #:description "name or name with formal parameters"
        (pattern name:id
                 #:with formals #'()
                 #:attr arity (make arity 0 0 null null))
        (pattern (name:id . formals)
                 #:attr arity (parse-kw-formals #'formals #:context stx))))
     (syntax-parse stx
       [(define-conventions h:header rule ...)
        (let ()
          (define rules (check-conventions-rules #'(rule ...) stx))
          (define rxs   (map car  rules))
          (define dens0 (map cadr rules))
          (define den+defs-list
            (let loop ((dens0 dens0))
              (if (pair? dens0)
                  (cons
                   (let ((den0 (car dens0)))
                     (let-values ([(den defs) (create-aux-def den0)])
                       (cons den defs)))
                   (loop (cdr dens0)))
                  '())))

          (define dens (map car den+defs-list))
          (define defs (apply append (map cdr den+defs-list)))

          (with-syntax (((rx ...) rxs)
                        (get-parsers
                         (datum->syntax stx (gensym "get-parsers")))
                        ((def ...) defs)
                        ((parser ...)
                         (map den:delayed-parser dens))
                        ((class-name ...)
                         (map den:delayed-class dens)))

            #'(begin
                (define-syntax h.name
                  (conv-in-lambda
                   (make-conventions
                    (datum->syntax #'h.name 'get-parsers)
                    (lambda ()
                      (let ([class-names (list #'class-name ...)])
                        (map list
                             (list 'rx ...)
                             (map make-den:delayed
                                  (generate-temporaries class-names)
                                  class-names)))))))
                (define get-parsers
                  (rlambda formals
                    def ...
                    (list parser ...))))))]))))

(define-syntax with-syntax-conventions 
  (lambda (stx)
    (with-syntax-class 
     ((header       
        #:description "name or name with formal parameters"
        (pattern name:id
                 #:with formals #'()
                 #:attr arity (make arity 0 0 null null))
        (pattern (name:id . formals)
                 #:attr arity (parse-kw-formals #'formals #:context stx))))
     (syntax-parse stx
       [(_ () code ...) 
        #'(begin code ...)]

       [(_ ((h:header rule ...) . l) code ...)
        (let ()
          (define rules (check-conventions-rules #'(rule ...) stx))
          (define rxs   (map car  rules))
          (define dens0 (map cadr rules))
          (define den+defs-list
            (let loop ((dens0 dens0))
              (if (pair? dens0)
                  (cons
                   (let ((den0 (car dens0)))
                     (let-values ([(den defs) (create-aux-def den0)])
                       (cons den defs)))
                   (loop (cdr dens0)))
                  '())))

          (define dens (map car den+defs-list))
          (define defs (apply append (map cdr den+defs-list)))

          (with-syntax (((rx  ...) rxs)
                        (get-parsers
                         (datum->syntax stx (gensym "get-parsers")))
                        ((def ...) defs)
                        ((parser ...)
                         (map den:delayed-parser dens))
                        ((class-name ...)
                         (map den:delayed-class dens)))

            #'(begin
                (define get-parsers
                  (rlambda formals
                    def ...
                    (list parser ...)))
                (let ()
                  (define-syntax h.name
                    (conv-in-lambda
                     (make-conventions
                      #'get-parsers
                      (lambda ()
                        (let ([class-names (list #'class-name ...)])
                          (map list
                               (list 'rx ...)
                               (map make-den:delayed
                                    (generate-temporaries class-names)
                                    class-names)))))))
                  (with-syntax-conventions l code ...)))))]))))

(define-syntax bdd?
  (lambda (x)
    (syntax-case x ()
      ((_ name x)
       (if (identifier-binding #'x 0)
           #t
           (raise-syntax-error 
            #f "literal in literalset is unbound"
            #'name #'x))
       #'#t))))

(define-syntax define-literal-set 
  (lambda (stx)
    (syntax-case stx ()
      [(define-literal-set name (lit ...))
       (begin
         (unless (identifier? #'name)
           (raise-syntax-error #f "expected identifier" stx #'name))
         (let ([lits (check-literals-list/litset #'(lit ...) stx)])
           (with-syntax ([((internal external) ...) lits])
             #`(define-syntax name
                 (litset-in-lambda
                  (begin
                    (bdd? name external) ...
                    (make-literalset
                     (list (list 'internal #'external) ...)
                     #f)))))))])))

(define-syntax with-syntax-literal-set 
  (lambda (stx)
    (syntax-case stx ()
      [(_ ((name (lit ...)) . l) . code)
       (begin
         (unless (identifier? #'name)
           (raise-syntax-error #f "expected identifier" stx #'name))
         (let ([lits (check-literals-list/litset #'(lit ...) stx)])
           (with-syntax ([((internal external) ...) lits])
             #`(let ()
                 (define-syntax name
                   (litset-in-lambda
                    (begin
                      (bdd? name external) ...
                      (make-literalset
                       (list (list 'internal #'external) ...)
                       #f))))
                 (with-syntax-literal-set l . code)))))])))
#;
(define-syntax (phase-of-enclosing-module stx)
  (syntax-case stx ()
    [(poem)
     (let ([phase-within-module (syntax-local-phase-level)])
       #`(let ([phase-of-this-expression
                (variable-reference->phase (%variable-reference))])
           (- phase-of-this-expression
              #,(if (zero? phase-within-module) 0 1))))]))

#|
Literal sets: The goal is for literals to refer to their bindings at

  phase 0 relative to the enclosing module

Use cases, explained:
1) module X with def-lit-set is required-for-syntax
     phase-of-mod-inst = 1
     phase-of-def = 0
     literals looked up at abs phase 1
       which is phase 0 rel to module X
2) module X with local def-lit-set within define-syntax
     phase-of-mod-inst = 1 (mod at 0, but +1 within define-syntax)
     phase-of-def = 1
     literals looked up at abs phase 0
       which is phase 0 rel to module X
3) module X with def-lit-set in phase-2 position (really uncommon case!)
     phase-of-mod-inst = 1 (not 2, apparently)
     phase-of-def = 2
     literals looked up at abs phase 0
       (that's why the weird (if (z?) 0 1) term)
|#


;; Literal sets
#;
(define-literal-set kernel-literals
  (begin
   begin0
   define-values
   define-syntaxes
   define-values-for-syntax
   set!
   let-values
   letrec-values
   %plain-lambda
   case-lambda
   if
   quote
   quote-syntax
   letrec-syntaxes+values
   with-continuation-mark
   %expression
   %plain-app
   %top
   %datum
   %variable-reference
   odule %provide %require
   %plain-module-begin))
