;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse src rep-attrs)
  #:use-module (syntax id-table)
  #:use-module (compat racket misc))
  

#|
#lang racket/base
(require racket/contract/base
         syntax/id-table
         unstable/syntax
         unstable/struct)
(provide (struct-out attr))
|#

#|
An IAttr is (make-attr identifier number boolean)
An SAttr is (make-attr symbol number boolean)

The number is the ellipsis nesting depth. The boolean is true iff the
attr is guaranteed to be bound to a value which is a syntax object (or
a list^depth of syntax objects).
|#

#|
SAttr lists are always stored in sorted order, to make comparison
of signatures easier for reified syntax-classes.
|#

(define-struct attr (name depth syntax?))
(struct-out attr)

(define (iattr? a)
  (and (attr? a) (identifier? (attr-name a))))

(define (sattr? a)
  (and (attr? a) (symbol? (syntax->datum (attr-name a)))))

;; increase-depth : Attr -> Attr
(define (increase-depth x)
  (make attr (attr-name x) (add1 (attr-depth x)) (attr-syntax? x)))

(export
 iattr?
 sattr?

 increase-depth
 
 attr-make-uncertain
 
 ;; IAttr operations
 append-iattrs
 union-iattrs
 reorder-iattrs
 rename-attr
 
 ;; SAttr operations
 iattr->sattr
 iattrs->sattrs
 sort-sattrs
 intersect-sattrss
 check-iattrs-subset)
 
;; IAttr operations

;; append-iattrs : (listof (listof IAttr)) -> (listof IAttr)
(define (append-iattrs attrss)
  (let* ([all (apply append attrss)]
         [names (map attr-name all)]
         [dup (check-duplicate-identifier names)])
    (when dup
      (wrong-syntax dup "duplicate attribute"))
    all))

;; union-iattrs : (listof (listof IAttr)) -> (listof IAttr)
(define (union-iattrs attrss)
  (define count-t (make-bound-id-table))
  (define attr-t (make-bound-id-table))
  (define list-count (length attrss))
  (let loop1 ((attrss attrss))
    (if (pair? attrss)
        (let ((attrs (car attrss)))
          (let loop2 ((attrs attrs))
            (if (pair? attrs)
                (let ((attr (car attrs)))
                  (define name (attr-name attr))
                  (define prev (bound-id-table-ref attr-t name #f))
                  (bound-id-table-set! attr-t name (join-attrs attr prev))
                  (let ([pc (bound-id-table-ref count-t name 0)])
                    (bound-id-table-set! count-t name (add1 pc)))
                  (loop2 (cdr attrs)))))                
          (loop1 (cdr attrss)))))


  (map (lambda (a)
         (if (= (bound-id-table-ref count-t (attr-name a)) list-count)
             a
             (attr-make-uncertain a)))
       (bound-id-table-map (lambda (_ v) v) attr-t)))

;; join-attrs : Attr Attr/#f -> Attr
;; Works with both IAttrs and SAttrs.
;; Assumes attrs have same name.
(define (join-attrs a b)
  (if (and a b)
      (proper-join-attrs a b)
      (or a b)))

(define (proper-join-attrs a b)
  (let ([aname (attr-name a)])
    (unless (equal? (attr-depth a) (attr-depth b))
      (wrong-syntax (and (syntax? aname) aname)
                    "attribute '~a' occurs with different nesting depth"
                    (if (syntax? aname) (syntax-e aname) aname)))
    (make attr aname (attr-depth a) (and (attr-syntax? a) (attr-syntax? b)))))

(define (attr-make-uncertain a)
  (make attr (attr-name a) (attr-depth a) #f))

(define (iattr->sattr a) a)

(define (iattrs->sattrs as)
  (sort-sattrs (map iattr->sattr as)))

  (define (sattr-name x) 
    (let ((x (attr-name x)))
      (if (syntax? x) (syntax->datum x) x)))

(define (sort-sattrs as)
  (let ((key (lambda (a) (symbol->string (sattr-name a)))))
    (sort as (lambda (x y) (string<? (key x) (key y))))))

(define (rename-attr a name)
  (make attr name (attr-depth a) (attr-syntax? a)))

;; intersect-sattrss : (listof (listof SAttr)) -> (listof SAttr)
;; FIXME: rely on sorted inputs, simplify algorithm and avoid second sort?
(define (intersect-sattrss attrss) 
 (cond [(null? attrss) null]
        [else
         (let* ([namess (map (lambda (attrs) (map sattr-name attrs)) attrss)]
                [names  (filter (lambda (s)
                                  (andmap (lambda (names) (memq s names))
                                          (cdr namess)))
                                (car namess))]
                [ht  (make-hash-table)]
                [put (lambda (attr) (hashq-set! ht (sattr-name attr) attr))]
                [fetch-like (lambda (attr) 
                              (hashq-ref ht (sattr-name attr) #f))])
           (let loop ([attrss attrss])
             (if (pair? attrss)
                 (begin
                   (let loop ((attrs (car attrss)))
                     (if (pair? attrs)
                         (let ((attr (car attrs)))
                           (if (memq (sattr-name attr) names)
                               (put (join-attrs attr (fetch-like attr))))
                           (loop (cdr attrs)))))
                   (loop (cdr attrss)))))
           (sort-sattrs (hash-map->list (lambda (k v) v) ht)))]))

;; reorder-iattrs : (listof SAttr) (listof IAttr) -> (listof IAttr)
;; Reorders iattrs (and restricts) based on relsattrs
;; If a relsattr is not found, or if depth or contents mismatches, raises error.
(define (reorder-iattrs relsattrs iattrs)
  (let ([ht (make-hash-table)])
    (for-each (lambda (iattr)
                (let ([remap-name (syntax-e (attr-name iattr))])
                  (hashq-set! ht remap-name iattr)))
                
              iattrs)
    (let loop ([relsattrs relsattrs])
      (if (null? relsattrs)
          null
          (let ([sattr (car relsattrs)]
                [rest (cdr relsattrs)])
            (let ([iattr (hashq-ref ht (attr-name sattr) #f)])
              (check-iattr-satisfies-sattr iattr sattr)
              (cons iattr (loop rest))))))))

(define (check-iattr-satisfies-sattr iattr sattr)
  (unless iattr
    (wrong-syntax #f "required attribute is not defined: ~s" 
                  (sattr-name sattr)))
  (unless (= (attr-depth iattr) (attr-depth sattr))
    (wrong-syntax (attr-name iattr)
                  "attribute has wrong depth (expected ~s, found ~s)"
                  (attr-depth sattr) (attr-depth iattr)))
  (when (and (attr-syntax? sattr) (not (attr-syntax? iattr)))
    (wrong-syntax (attr-name iattr)
                  "attribute may not be bound to syntax: ~s"
                  (sattr-name sattr))))

;; check-iattrs-subset : (listof IAttr) (listof IAttr) stx -> void
(define (check-iattrs-subset little big ctx)
  (let ()
    (define big-t (make-bound-id-table))
    (for-each (lambda (a)
                (bound-id-table-set! big-t (attr-name a) #t))              
              big)

    (for-each (lambda (a)
                (unless (bound-id-table-ref big-t (attr-name a) #f)
                  (raise-syntax-error 
                   #f
                   "attribute bound in defaults but not in pattern"
                   ctx
                   (attr-name a))))
              little)))


