;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse debug)
  #:use-module (syntax parse src rep-data)
  #:use-module (syntax parse src rep)
  #:use-module (syntax parse src kws)

  #:use-module (compat racket misc)

  #:use-module (syntax parse)
  #:use-module (syntax parse src runtime)
  #:use-module (syntax parse src runtime-progress)
  #:use-module (syntax parse src runtime-report)

  #:use-module (srfi srfi-11)

  #:export (syntax-class-parse
            syntax-class-attributes
            syntax-class-arity
            syntax-class-keywords

            debug-rhs
            debug-pattern
            debug-parse))
#|
#lang racket/base
(require (for-syntax racket/base
                     syntax/stx
                     unstable/syntax
                     "private/rep-data.rkt"
                     "private/rep.rkt"
                     "private/kws.rkt")
         "../parse.rkt"
         "private/runtime.rkt"
         "private/runtime-progress.rkt"
         "private/runtime-report.rkt"
         "private/kws.rkt")

(provide syntax-class-parse
         syntax-class-attributes
         syntax-class-arity
         syntax-class-keywords

         debug-rhs
         debug-pattern
         debug-parse)
|#

(define-syntax syntax-class-parse 
  (lambda (stx)
    (syntax-case stx ()
      [(_ s x arg ...)
       (with-fluids ((current-syntax-context stx))
         (let* ([argu (parse-argu (syntax->list #'(arg ...)) #:context stx)]
                [stxclass
                 (get-stxclass/check-arity #'s stx
                                           (length (arguments-pargs argu))
                                           (arguments-kws argu))]
                [attrs (stxclass-attrs stxclass)])
           (with-syntax ([parser (stxclass-parser stxclass)]
                         [argu argu]
                         [(name ...) (map attr-name attrs)]
                         [(depth ...) (map attr-depth attrs)])
             #'(let ([fh (lambda (fs) fs)])
                 (app-argu parser x x (ps-empty x x) null fh fh
                           (lambda (fh cp . attr-values)
                             (map vector '(name ...) '(depth ...) attr-values))
                           argu)))))])))

(eval-when (compile load eval)
           (define (mk handler)
             (lambda (stx)
               (syntax-case stx ()
                 [(_ s)
                  (with-fluids ((current-syntax-context stx))
                    (handler (get-stxclass #'s)))]))))
  

(define-syntax syntax-class-attributes  
  (mk (lambda (s)
        (let ([attrs (stxclass-attrs s)])
          (with-syntax ([(a ...) (map (lambda (x)
                                        (datum->syntax #'1 x))
                                      (map attr-name attrs)) ]
                        [(d ...) (map attr-depth attrs)])
            #'(quote ((a d) ...)))))))

(define-syntax syntax-class-arity
  (mk (lambda (s)
        (let ([a (stxclass-arity s)])
          #`(to-procedure-arity '#,(arity-minpos a) '#,(arity-maxpos a))))))

(define-syntax syntax-class-keywords
  (mk (lambda (s)
        (let ([a (stxclass-arity s)])
          #`(values '#,(arity-minkws a) '#,(arity-maxkws a))))))


(define-syntax debug-rhs 
  (lambda (stx)
    (syntax-case stx ()
      [(debug-rhs rhs)
       (let ([rhs (parse-rhs #'rhs #f #f #:context stx)])
         #`(quote #,rhs))])))


(define-syntax debug-pattern 
  (lambda (stx)
    (syntax-case stx ()
      [(debug-pattern p . rest)
       (let-values ([(rrest pattern defs)
                     (parse-pattern+sides #'p #'rest
                                          #:splicing? #f
                                          #:decls (new-declenv null)
                                          #:context stx)])
         (unless (stx-null? rrest)
           (raise-syntax-error #f "unexpected terms" stx rrest))
         #`(quote ((definitions . #,defs)
                   (pattern #,pattern))))])))

(define-syntax debug-parse 
  (syntax-rules ()
    ((_ x p ...)
     (let/ec escape
       (with-fluids ((current-failure-handler
                     (lambda (_ fs)
                       (escape
                        `(parse-failure
                          #:raw-failures
                          ,(pku (failureset->sexpr fs) #f)
                          #:maximal-failures
                          ,(let ([selected 
                                  (map (lambda (fs)
                                         (cons 'equivalence-class
                                               (map failure->sexpr fs)))
                                       (maximal-failures fs))])
                             (if (= (length selected) 1)
                                 (car selected)
                                 (cons 'union selected))))))))
                   (syntax-parse x [p 'success] ...))))))

