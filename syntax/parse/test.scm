;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(use-modules (syntax parse))
(use-modules (compat racket misc))

(define-syntax m1
  (lambda (x)
    (syntax-parse x 
      (x #''x))))
(format #t "m1: m1 == ~a~%" m1)

(define-syntax m2
  (lambda (x)
    (syntax-parse x 
      ((_ x) #'x))))
(format #t "m2: 1 == ~a~%" (m2 1))

(define-syntax m3
  (lambda (x)
    (syntax-parse x 
      ((_ x ...) #'(+ x ...)))))
(format #t "m3: 3 == ~a~%" (m3 1 2))

(define-syntax m4
  (lambda (x)
    (syntax-parse x 
      ((_ x ... y) #'(+ x ...)))))
(format #t "m4: 3 == ~a~%" (m4 1 2 3))

(define-syntax m5
  (lambda (x)
    (syntax-parse x 
      ((_ x . l) #'(+ x . l)))))
(format #t "m5: 3 == ~a~%" (m5 1 2))

(define-syntax m6
  (lambda (x)
    (syntax-parse x 
      ((_ (x ...)) #'(+ x ...))
      ((_ x y ...) #'(+ y ...)))))
(format #t "m6: 3 == ~a~%" (m6 (1 2)))
(format #t "m6: 3 == ~a~%" (m6 3 2 1))

(define-syntax m7
  (lambda (x)
    (syntax-parse x
      ((_ x:id) #''x))))
(format #t "m7: a == ~a~%" (m7 a))


(define-syntax m8
  (lambda (x)
    (syntax-parse x
      ((_ y:str) #'y))))
(format #t "m8: 'a' == ~a~%" (m8 "a"))

(define-syntax m9
  (lambda (x)
    (syntax-parse x
      ((_ x:number) #'x))))
(format #t "m9: 1 == ~a~%" (m9 1))

(define-syntax m10
  (lambda (x)
    (syntax-parse x
      ((_ x:keyword) #'x))))
(format #t "m10: #:a == ~a~%" (m10 #:a))

(define-syntax m11
  (lambda (x)
    (syntax-parse x
      ((_ x:expr) #''x))))
(format #t "m11: a == ~a~%" (m11 a))

(define-syntax m12
  (lambda (x)
    (syntax-parse x
      ((_ x:boolean) #'x))))
(format #t "m12: #t == ~a~%" (m12 #t))

(define-syntax m13
  (lambda (x)
    (syntax-parse x
      ((_ x:char) #'x))))
(format #t "m13: a == ~a~%" (m13 #\a))

(define-syntax m14
  (lambda (x)
    (syntax-parse x
      ((_ . (~and (x ...) (a:id b:number))) #''(b x ... a)))))

(format #t "m14: (1 a 1 a) == ~a~%" (m14 a 1))

(define-syntax m15
  (lambda (x)
    (syntax-parse x
      ((_ . (~or (x) (x a))) 
       (if (attribute a)
           (with-syntax ((k a))
             #''(x k))
           #''(x #f))))))

(format #t "m15: (a #f) == ~a~%" (m15 a))
(format #t "m15: (a b) == ~a~%" (m15 a b))

(define-syntax m16
  (lambda (x)
    (syntax-parse x
      ((_ (~seq 1 a) ...) #''(a ...)))))
(format #t "m16: (a b c) = ~a~%" (m16 1 a 1 b 1 c))

(define-syntax m17
  (lambda (x)
    (syntax-parse x
      ((_ (~not 1)) #''(not 1))
      ((_       1)  #'1))))

(format #t "m17: (not 1) = ~a~%" (m17 2))
(format #t "m17: 1 = ~a~%"       (m17 1))

(define-syntax-class t1_ (pattern (k:id v:number)))

(define-syntax m18
  (lambda (x)
    (syntax-parse x
      ((_ x:t1_) #''(x.k x.v)))))

(format #t "m18: (a 1) = ~a~%" (m18 (a 1)))


(define-syntax m19
  (lambda (x)
    (syntax-parse x
      ((_ x:t1_ ...) #''(x.k ... x.v ...)))))
(format #t "m19: (a b 1 2) = ~a~%" (m19 (a 1) (b 2)))


(define-syntax-class t2 (pattern ((k:id v:number) ...)))

(define-syntax m20
  (lambda (x)
    (syntax-parse x
      ((_ . x:t2) #''(x.k ... x.v ...)))))
(format #t "m20: (a b 1 2) = ~a~%" (m20 (a 1) (b 2)))

(define-syntax-class t3 (pattern (y:t1_ ...)))
(define-syntax m21
  (lambda (x)
    (syntax-parse x
      ((_ . x:t3) #''(x.y ...)))))
(format #t "m21: ((a 1) (b 2)) = ~a~%" (m21 (a 1) (b 2)))

(define plus  1)
(define times 1)
(define-syntax-class arith
  #:literals (plus times)
  (pattern n:nat
           #:with expr #'n)
  (pattern (plus a:arith b:arith)
           #:with expr #'(+ a.expr b.expr))
  (pattern (times a:arith b:arith)
           #:with expr #'(* a.expr b.expr)))


(define-syntax m22
  (lambda (x)
    (syntax-parse x 
      ((_ m:arith) #'(list 'm.expr '= m.expr)))))


(let ((x (m22 (times (plus 2 3) 2))))
  (format #t "m22: 10 = ~a~%" x))

(define-syntax m23
  (lambda (x)
    (syntax-parse x
      ((_ #:a) #'#f)
      ((_ #:b) #'#t))))

(format #t "m23: #t = ~a~%" (m23 #:b))

(define-syntax m24
  (lambda (x)
    (syntax-parse x
      ((_ (~datum a)) #'#t))))
(format #t "m24: #t = ~a~%" (m24 a))

(define-syntax m25
  (lambda (x)
    (syntax-parse x
      ((_ (~datum (a b))) #'#t))))
(format #t "m25: #t = ~a~%" (m25 (a b)))


(define-syntax mycond 
  (lambda (stx)
    (syntax-parse stx
      [(mycond (~or (~seq #:error-on-fallthrough who:expr) (~seq))
               clause ...)
       (with-syntax ([error? (if who #'#t  #'#f)]
                     [who    who])
         #'(mycond* error? who clause ...))])))

(define-syntax mycond*
  (lambda (x)
    (syntax-case x ()
      [(mycond error? who [question answer] . clauses)
       #'(if question answer (mycond* error? who . clauses))]
      [(mycond #t who)
       #'(rerror who "no clauses matched")]
      [(mycond #f _)
       #'(if #f #f)])))

(define-syntax mycond2
  (lambda (stx)  
    (with-syntax-data 
     ((splicing maybe-fallthrough-option
        (pattern (~seq #:error-on-fallthrough who:expr)
                 #:with error? #'#t)
        (pattern (~seq)
                 #:with error? #'#f
                 #:with who    #'#f)))
     (syntax-parse stx
       [(mycond fo:maybe-fallthrough-option clause ...)
        #'(mycond* fo.error? fo.who clause ...)]))))

(format #t "mycond  #t = ~a~%" 
        (mycond #:error-on-fallthrough 'test
                (#t #t)))

(format #t "mycond2 #t = ~a~%" 
        (mycond2 #:error-on-fallthrough 'test
                (#t #t)))


(define-syntax-class maybe-renamed
  #:attributes (internal external)
  (pattern internal:id
           #:with external #'internal)
  (pattern (internal:id external:id)))

(define-syntax-class init-decl
  #:attributes (internal external default)
  (pattern internal:id
           #:with external #'internal
           #:with default #'())
  (pattern (mr:maybe-renamed)
           #:with internal #'mr.internal
           #:with external #'mr.external
           #:with default #'())
  (pattern (mr:maybe-renamed default0:expr)
           #:with internal #'mr.internal
           #:with external #'mr.external
           #:with default #'(default0)))

(define-syntax m26
  (lambda (x)
    (syntax-parse x
      ((_ x:init-decl) #''(x.internal x.external x.default)))))

(format #t "m26: (a a ())  = ~a~%" (m26 a))
(format #t "m26: (a a (b)) = ~a~%" (m26 (a b)))
(format #t "m26: (a b ())  = ~a~%" (m26 ((a b))))
(format #t "m26: (a b (c)) = ~a~%" (m26 ((a b) c)))

(define-struct-struct bind-clause (vars seq-expr))
(define-struct-struct when-clause (guard))

(define-splicing-syntax-class for-clause
  #:attributes (norm)
  (pattern [var:id seq:expr]
           #:with norm #'[(var) seq])
  (pattern [(var:id ...) seq:expr]
           #:with norm #'[(var ...) seq])
  (pattern (~seq #:when guard:expr)
           #:with norm #'[#:when guard]))

(define-syntax m27
  (lambda (x)
    (syntax-parse x
      ((_ x:for-clause) #''x.norm))))
(format #t "m27: ~a~%" (m27 (a (+ 1 2))))
(format #t "m27: ~a~%" (m27 ((a b c) (+ 1 2))))
(format #t "m27: ~a~%" (m27 #:when (+ 1 2)))

(define-splicing-syntax-class for-clause2
  #:attributes (ast)
  (pattern [var:id seq:expr]
           #:attr ast (make bind-clause (list #'var) #'seq))
  (pattern [(var:id ...) seq:expr]
           #:attr ast (make bind-clause (syntax->list #'(var ...))
                                   #'seq))
  (pattern (~seq #:when guard:expr)
           #:attr ast (make when-clause #'guard)))

(define-syntax m28
  (lambda (y)
    (syntax-parse y
      ((_ x:for-clause2) 
       (with-syntax ((name (datum->syntax 
                            #'y
                            (struct-vtable-name  
                             (struct-vtable x.ast)))))
         #''name)))))
(format #t "m28: bind-clause = ~a~%" (m28 (a (+ 1 2))))
(format #t "m28: bind-clause = ~a~%" (m28 ((a b c) (+ 1 2))))
(format #t "m28: when-clause = ~a~%" (m28 #:when (+ 1 2)))

(define-splicing-syntax-class maybe-super
    (pattern (~seq super:id))
    (pattern (~seq)))

(define-syntax-class field-option
    (pattern #:mutable)
    (pattern #:auto))

(define-syntax-class field
  (pattern field:id
           #:with (option ...) '())
  (pattern [field:id option:field-option ...]))

(define-syntax m29
  (lambda (x)
    (syntax-parse x
      ((struct name:id super:maybe-super (field:field ...)
               (~or (~optional
                     (~or (~seq #:inspector inspector:expr)
                          (~seq (~and #:transparent transparent-kw))
                          (~seq (~and #:prefab prefab-kw)))
                     #:name "#:inspector, #:transparent, or #:prefab option")
                    (~optional (~seq (~and #:mutable) mutable-kw)
                               #:name "#:mutable option")
                    (~optional (~seq #:super super-expr:expr)
                               #:name "#:super option")
                    (~optional (~seq #:auto-value auto:expr)
                               #:name "#:auto-value option")
                    (~optional (~seq #:guard guard:expr)
                               #:name "#:guard option")
                    (~seq #:property prop:expr prop-val:expr)
                    (~optional (~seq #:constructor-name constructor-name:id)
                               #:name "#:constructor-name option")
                    (~optional
                     (~seq #:extra-constructor-name extra-constructor-name:id)
                     #:name "#:extra-constructor-name option")
                    (~optional (~seq (~and #:omit-define-syntaxes 
                                           omit-def-stxs-kw))
                               #:name "#:omit-define-syntaxes option")
                    (~optional (~seq (~and #:omit-define-values 
                                           omit-def-vals-kw))
                               #:name "#:omit-define-values option")) ...)
       #'#f))))
   
(define-syntax-class star
  #:description "*"
  (pattern star:id
           #:fail-unless (eq? '* (syntax-e #'star)) "missing *")
  (pattern star:id
           #:fail-unless (eq? '...* (syntax-e #'star)) "missing ...*"))

(define-syntax-class ddd
  #:description "..."
  (pattern ddd:id
           #:fail-unless (eq? '... (syntax-e #'ddd)) "missing ..."))


(define-splicing-syntax-class ddd/bound
  #:description "... followed by variable name"
  #:attributes (bound)
  (pattern i:id
           #:attr s (symbol->string (syntax-e #'i))
           #:fail-unless (> (string-length (attribute s)) 3) 
                            #f
           #:fail-unless (equal? "..." (substring (attribute s) 0 3)) 
                            "missing ..."
           #:attr bound (datum->syntax #'i (string->symbol (substring (attribute s) 3))))

  (pattern (~seq _:ddd bound:id)))

(define-syntax m30 
  (lambda (x)
    (syntax-parse x 
      ((_ x:ddd/bound) (with-syntax ((i x.bound)) #''i)))))
       
(format #t "m30: a = ~a~%" (m30 ...a))
(format #t "m30: a = ~a~%" (m30 ... a))

#;
(format #t "m31: ok = ~a~%"
        (syntax-parse #'(a b c) 
          #:disable-colon-notation
          [(x:y ...) 'ok]))

(define-syntax-class (nat> bound)
  (pattern n:nat
           #:fail-unless (> (syntax-e #'n) bound)
           (format "expected number > ~s" bound)))

(define-syntax-class (natlist> bound)
  #:local-conventions ([N (nat> bound)])
  (pattern (N ...)))

(define (m31 bound x)
    (syntax-parse x
      #:local-conventions ([NS (natlist> bound)])
      [NS 'ok]))
(format #t "m31: ok = ~a~%" (m31 0 #'(1 2 3)))

(define-conventions xyz-as-ids
    [x id] [y id] [z id])

(format #t "conv: (a b c) = ~a~%"
        (syntax-parse #'(a b c 1 2 3)
          #:conventions (xyz-as-ids)
          [(x ... n ...) (syntax->datum #'(x ...))]))


(define-conventions xn-prefixes
    ["^x" id ]
    ["^n" nat])

(format #t "conv: (a (b c) 1 (2 3)) = ~a~%"
        (syntax-parse #'(a b c 1 2 3)
          #:conventions (xn-prefixes)
          [(x0 x ... n0 n ...)
           (syntax->datum #'(x0 (x ...) n0 (n ...)))]))
   

(define-literal-set binops (+ - * /))

(define-syntax m32
  (lambda (x)
    (syntax-parse x
      #:literal-sets (binops)
      #:conventions (xn-prefixes)
      ((_ + n1 n2) #''(plus  n1 n2))
      ((_ * n1 n2) #''(mult  n1 n2))
      ((_ - n1 n2) #''(minus n1 n2))
      ((_ / n1 n2) #''(div   n1 n2)))))

(format #t "m32: (minus 1 2) = ~a~%" (m32 - 1 2))

(define-syntax-class two
  (pattern (x y)))

(format #t "~~var: ((a b) a b) = ~a~%"
        (syntax-parse #'(a b)
          [(~var t two) (syntax->datum #'(t t.x t.y))]))

(define-syntax-class (nat-less-than n)
  (pattern x:nat #:when (< (syntax-e #'x) n)))

(define-syntax m33
  (lambda (x)
    (syntax-parse x
      [(_ (~var small (nat-less-than 4)) ... large:nat ...)
       #''((small ...) (large ...))])))

(format #t "m33: ((1 2 3) (4 5 6)) = ~a~%" (m33 1 2 3 4 5 6))
(format #t "...+: none = ~a~%"
        (syntax-parse #'()
          [(n:nat ...+) 'ok]
          [_            'none]))

(format #t "#(...): 3 = ~a~%"
        (syntax-parse #'#(1 2 3)
          [#(x y z) (syntax->datum #'z)]))

(format #t "#( ... ~~rest y) (2 3) = ~a~%"
        (syntax-parse #'#(1 2 3)
          [#(x ~rest y) (syntax->datum #'y)]))

(define-syntax m34
  (lambda (x)
    (syntax-parse x
      ((~describe "to be no 1" (_ 1)) #t))))

(format #t "~~and H: ((#:a #:b) (1 2) (#:a 1 #:b 2)) = ~a~%"
        (syntax-parse #'(#:a 1 #:b 2 3 4 5)
          [((~and (~seq (~seq k:keyword e:expr) ...)
                  (~seq keyword-stuff ...))
            positional-stuff ...)
           (syntax->datum #'((k ...) (e ...) (keyword-stuff ...)))]))

(define-syntax m35
  (lambda (x)
    (syntax-parse x
      [(_ (~optional (~seq #:foo x) #:defaults ([x #'#f])) y:id ...)
       (attribute x)])))

(format #t "m35: 2 = ~a~%"
        (m35 #:foo 2 a b c))
(format #t "m35: #f = ~a~%"
        (m35 m a b c))

;this does not work n and s is not pattern variables

(define-syntax m36
  (lambda (x)
    (syntax-parse x
      [(_ (~optional (~seq #:nums n:nat ...) #:defaults ([(n 1) #'()]))
          (~optional (~seq #:syms s:id ...)  #:defaults ([(s 1) #'()])))
       #''((n ...) (s ...))])))

(format #t "m36: ((1 2 3) (a b c)) = ~a~%"
        (m36 #:nums 1 2 3 #:syms a b c))

(define-splicing-syntax-class nf-id ; non-final id
    (pattern (~seq x:id (~peek another:id))))

(format #t "~~peek: ((a b) (c 1 2 3)) = ~a~%"
        (syntax-parse #'(a b c 1 2 3)
          [(n:nf-id ... rest ...)
           (list (syntax->datum #'(n.x ...))
                 (syntax->datum #'(rest ...)))]))

(define parser1
  (syntax-parser
   [((~or (~once (~seq #:a x) #:name "#:a keyword")
          (~optional (~seq #:b y) #:name "#:b keyword")
          (~seq #:c z)) ...)
    'ok]))


;BUGG this does not give correct error:
;(parser1 #'(#:a 1 #:a 2))

(define-syntax m37
  (lambda (x)
    (syntax-parse x
      #:literals (+ -)
      [(_ + ~! (x:id ...) e) #''+]
      [(_ - ~! (x:id ...) e) #''-]
      [(_ . e) #''expression])))

(format #t "m37: - = ~a~%" (m37 - (a b) 1))
(format #t "m37: error expected identifier =~%
 ~a~%" (catch #t
         (lambda () (macroexpand '(m37 - a 1)))
         (lambda x x)))
