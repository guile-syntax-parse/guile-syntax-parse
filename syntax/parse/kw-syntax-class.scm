;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (syntax parse kw-syntax-class)
  #:use-module (syntax parse)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:export  (define-kw-splicing-class))

#|
Use this as

(define-kw-splicing-class x (a 0) (b #f))

(syntax-parse #'((#:b 1) (#:b 2))
  (((a:x) ((~var b (x #:a 1))))
   ...))

a.a = 0
a.b = 1
b.a = 1
b.b = 2
|#

(define (pp x)
  #;(pretty-print
   (let loop ((x x))
     (syntax-case x ()
       ((x . l) (cons (loop #'x) (loop #'l)))
       (x       (syntax->datum #'x)))))
  x)

(define-syntax define-kw-splicing-class
  (lambda (x)
    (define (make-with keys skeys ukeys)
      (let loop ((ks keys) (sk skeys) (uk ukeys))
	(if (pair? ks)
	    #`(#:with #,(car ks)
		      (let ((w #'(#,(car sk) (... ...))))
			(if (pair? w)
			    (car w)
			    #,(car uk)))
	       #,@(loop (cdr ks) (cdr sk) (cdr uk)))
	    '())))
		   

    (define (combine-args as bs)
      (let loop ((as as) (bs bs))
	(if (pair? as)
	    (cons* (car as) (car bs) (loop (cdr as) (cdr bs)))
	    '())))

    (define (id->kkey x)
      (symbol->keyword (syntax->datum x)))

    (define-syntax with-syntax*
      (syntax-rules ()
	((_ () code ...) (begin code ...))
	((_ (x . l) . u)
	 (with-syntax (x) (with-syntax* l . u)))))

    (syntax-parse x
      ((_ name (key:id val:expr . (~and l (~or () (x)))) ...)
       (with-syntax* (((kkey ...) (map id->kkey #'(key ...)))
		      ((skey ...) (generate-temporaries #'(key ...)))
		      ((ukey ...) (generate-temporaries #'(key ...)))
		      ((with ...) (make-with #'(key ...) #'(skey ...)
					     #'(ukey ...)))
		      ((args ...) (combine-args #'(kkey ...) 
						#'((ukey val) ...))))
	 (pp #'(define-splicing-syntax-class (name args ...)
	       (pattern (~seq (~or (~seq kkey (~var skey . l)) ... )
			       (... ...))
		   #:fail-when 
		   (let loop ((sk (list #'(skey (... ...)) ...)))
		     (match sk
			    (((x y . _) . u) #t)
			    ((_         . u) (loop u))
			    (()              #f)))
		   "the same keyword was used multiple times"
		    
		   with 
		   ...))))))))
