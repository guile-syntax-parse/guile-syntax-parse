;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


;;HACK
(define-syntax-rule (define-module-- name args ...)
  (cond-expand
   (guile-3
    (define-module- name
      #:declarative? #f
      args ...))
   (guile
    (define-module- name args ...))))


(define-module-- (syntax parse)
  #:use-module (syntax parse src sc)
  #:use-module (syntax parse src lib)
  #:use-module (compat racket misc)
  #:use-module (syntax parse src litconv)
  #:use-module (syntax parse src with)
  #:use-module (syntax parse src keywords)
  #:export (table))
  

(re-export-all (syntax parse src sc ) #:except (parser/rhs))
(re-export-all (syntax parse src litconv))
(re-export-all (syntax parse src lib) #:except (static))
(re-export-all (syntax parse src with))
(re-export-all (syntax parse src keywords))


(let ((src (module-variable (resolve-module '(compat racket lambda)) 
			    'rlambda))
      (dst (module-variable (resolve-module '(syntax parse src parse))
			    'rlambda))
      (ds2 (module-variable (resolve-module '(syntax parse src litconv))
			    'rlambda)))
  (variable-set! dst
		 (variable-ref src))
  (variable-set! ds2
		 (variable-ref src)))

#|
lang racket/base
(require racket/contract/base
         "parse/private/sc.rkt"
         "parse/private/litconv.rkt"
         "parse/private/lib.rkt"
         "parse/experimental/provide.rkt")
(provide (except-out (all-from-out "parse/private/sc.rkt")
                     parser/rhs)
         (all-from-out "parse/private/litconv.rkt")
         (except-out (all-from-out "parse/private/lib.rkt")
                     static))


(provide-syntax-class/contract
 [static (syntax-class/c [(-> any/c any/c) (or/c string? symbol? #f)])])

(define-syntax-class table
  (pattern ((key value) ...)))

(define-syntax-class table
  (pattern ((key value))))

|#
