;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(use-modules (compat racket match))
(use-modules (ice-9 pretty-print))

(define-syntax-rule (test x y)
  (pr 'x (let ((xx x)) 
          (if (equal? xx y) 
              'PASS 
              `(FAIL ,xx ~ ,y)))))

(define (pr a b)
  (pretty-print (list a '---> b)))


(test 
 (match '(1 2 3)
   ((list a b  a) (list a b))
   ((list a b  c) (list c b a)))
 '(3 2 1))


(test 
 (match '(1 '(x y z) 1)
   [(list a b a) (list a b)]
   [(list a b  c) (list c b a)])
 '(1 '(x y z)))

(test
 (match '(1 2 3)
   [(list _ _ a) a])
 3)

(test
 (match "yes"
   ["no" #f]
   ["yes" #t])
 #t)

(test
 (match '(1 2 3)
   [(list a b c) (list c b a)])
 '(3 2 1))

(test
 (match '(1 2 3)
   [(list 1 a ...) a])
 '(2 3))

(test
 (match '(1 2 3)
   [(list 1 a ..3) a]
   [_ 'else])
 'else)

(test
 (match '(1 2 3 4)
   [(list 1 a ..3) a]
   [_ 'else])
 '(2 3 4))

(test
 (match '(1 2 3 4 5)
   [(list 1 a ..3 5) a]
   [_ 'else])
 '(2 3 4))

(test
 (match '(1 (2) (2) (2) 5)
   [(list 1 (list a) ..3 5) a]
   [_ 'else])
 '(2 2 2))

(test
 (match '(1 2 3 . 4)
   [(list-rest a b c d) d])
 4)

(test
 (match '(1 2 3 . 4)
   [(list-rest a ... d) (list a d)])
 '((1 2 3) 4))

(test
 (match '(1 2 3)
   [(list-no-order 3 2 x) x])
 1)


(test
 (match (pk (match '(1 2 3 4 5 6)
              [(list-no-order 6 2 y ...) y]))
	((list-no-order 1 3 4 5) #t))
 #t)
 

(test
 (match #(1 (2) (2) (2) 5)
   [(vector 1 (list a) ..3 5) a])
 '(2 2 2))

#;
(test
 (match hash(("a" . 1) ("b" . 2))
    [(hash-table ("b" b) ("a" a)) (list b a)])
 '(2 1))

#;
(test
 (match hash(("a" . 1) ("b" . 2))
   [(hash-table (key val) ...) key])
 '("b" "a"))

(test
 (match (cons 1 2)
   [(cons a b) (+ a b)])
 3)

#;
(begin
  (define-struct tree (val left right))
  (test
   (match (make-tree 0 (make-tree 1 #f #f) #f)
     [(tree a (tree b  _ _) _) (list a b)])
   '(0 1)))

(test
 (match "apple"
   [(regexp "p+") 'yes]
   [_ 'no])
 'yes)

(test
 (match "banana"
   [(regexp "p+") 'yes]
   [_ 'no])
 'no)

;;TODO be standard compliant here
(test
 (match "apple"
   [(regexp "p+(.)" "l") 'yes]
    [_ 'no])
 'yes)

(test
 (match "append"
   [(regexp "p+(.)" "l") 'yes]
   [_ 'no])
 'no)

(test
 (match '(1 (2 3) 4)
   [(list _ (and a (list _ ...)) _) a])
 '(2 3))

(test
 (match '(1 2)
   [(or (list a 1) (list a 2)) a])
 1)

(test
 (match '(1 2 3)
   [(list (not 4) ...) 'yes]
   [_ 'no])
 'yes)

(test
 (match '(1 4 3)
   [(list (not 4) ...) 'yes]
   [_ 'no])
 'no)

(test
 (match '(1 2)
   [(app length 2) 'yes])
 'yes)

(test
 (match '(1 3 5)
   [(list (? odd?) ...) 'yes])
 'yes)

(test
 (match '(1 2 3)
   [`(1 ,a ,(? odd? b)) (list a b)])
 '(2 3))

(test
 (match* (1 2 3)
   [(_ (? number?) x) (+ 1 x)])
 4)

(test
 (match '(1 (2 2) 1) 
   ((list _ (list _ _) _) #f))
 #f)

(test
 (match '(1 1 1 1)
	((list x x ...) x))
 1)

(test
 (match '(1 1 1 1)
	((list x (and x y) ...) y))
 '(1 1 1))