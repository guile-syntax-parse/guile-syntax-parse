;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


;; Copyright (C) 2012 Ian Price <ianprice90@googlemail.com>
;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

(package (syntax-parse (0 1))
  (provides racket-match)
  (depends (srfi-9) (srfi-11))
  (synopsis "syntax-parse port")
  (description
   "A port of racket's syntax-parse to guile."
   "Syntax parse is a macro framework proving a syntax pattern"
   "language, and automatic generation of error messages.")
  (homepage "http://gitorious.org/guile-syntax-parse")
  (libraries "compat" "syntax")
  (documentation "examples" "README" "COPYING.LIB" "LICENSE.TXT"))
